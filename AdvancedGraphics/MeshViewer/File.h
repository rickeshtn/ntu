#ifndef FILE_H
#define FILE_H

#include<string>
#include<fstream>
#include "ACG_DataTypes.h"

using namespace std;

class mFile
{
public:
	mFile();
	mFile(IN string strFileName);
	bool openMFile();
	bool isMfileOpen();
	bool closeMfile();
	string getFileName();
	string mStrFileName;

protected:
	fstream mFileSource;

private:
	bool mIsFileOpen;
};

#endif // FILE_H
