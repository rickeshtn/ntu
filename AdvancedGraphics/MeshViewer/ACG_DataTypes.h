#ifndef ACG_DATATYPES_H
#define ACG_DATATYPES_H

#include <vector>
#include <utility>
#include<glut.h>
#include<gl/GL.h>
#include "ACG_Vector3D.h"
using namespace std;


#define IN
#define OUT
#define INOUT

#define THREE_DIMENSION 3
#define MIN_VERT_FOR_FACE 3

enum CameraTypes{
    CAMERA_3D = 0,
    CAMERA_XZ,
    CAMERA_YZ,
	CAMERA_XY
};

struct ACG_HalfEdge_Edge;

struct ACG_HalfEdge_Vert
{
	//vertex ID
	unsigned m_vertId;
	//
	ACG_Vector3D m_vertInfo;

	ACG_HalfEdge_Edge* m_HalfEdge;

	ACG_Vector3D m_NormalVector;

	ACG_HalfEdge_Vert()
	{
		m_HalfEdge = NULL;	
	}

	//Some more things to add here
	static bool greaterByX(ACG_HalfEdge_Vert* vert1, ACG_HalfEdge_Vert* vert2)
	{
		return vert1->m_vertInfo.x > vert2->m_vertInfo.x;
	}

	static bool greaterByY(ACG_HalfEdge_Vert* vert1, ACG_HalfEdge_Vert* vert2)
	{
		return vert1->m_vertInfo.y > vert2->m_vertInfo.y;
	}

	static bool greaterByZ(ACG_HalfEdge_Vert* vert1, ACG_HalfEdge_Vert* vert2)
	{
		return vert1->m_vertInfo.z > vert2->m_vertInfo.z;
	}
};

struct ACG_HalfEdgeFace
{
	unsigned m_FaceId;

	ACG_HalfEdge_Edge* m_Edge;

	ACG_Vector3D m_FaceNormal;

	double m_Area;

	ACG_HalfEdgeFace()
	{
		m_Edge = NULL;
	}
};

struct ACG_HalfEdge_Edge
{
	ACG_HalfEdge_Edge* m_PairEdge;
	
	ACG_HalfEdge_Edge* m_NextEdge;

	ACG_HalfEdge_Edge* m_PrevEdge;

	ACG_HalfEdge_Vert* m_EndVert;
	
	ACG_HalfEdgeFace* m_IncidentFace;

	ACG_HalfEdge_Edge()
	{
		m_PairEdge = NULL;
		m_NextEdge = NULL;
		m_PrevEdge = NULL;
		m_EndVert = NULL;
	}


};


#endif // ACG_DATATYPES_H
