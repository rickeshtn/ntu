#include"ACG_Vector3D.h"
#include<math.h>

using namespace std;

ACG_Vector3D::ACG_Vector3D(double VerterX, double VertexY, double VertexZ)
{
    x = VerterX;
    y = VertexY;
    z = VertexZ;
}

ACG_Vector3D::ACG_Vector3D()
{
    x = 0;
    y = 0;
    z = 0;
}

ACG_Vector3D::~ACG_Vector3D()
{

}

ACG_Vector3D ACG_Vector3D::operator+(ACG_Vector3D& ACG_Vertex)
{
    return ACG_Vector3D(x + ACG_Vertex.x,y+ACG_Vertex.y,z+ACG_Vertex.z);
}

ACG_Vector3D ACG_Vector3D::operator-(ACG_Vector3D& ACG_Vertex)
{
	return ACG_Vector3D((x-ACG_Vertex.x),(y-ACG_Vertex.y),(z-ACG_Vertex.z));
}
ACG_Vector3D ACG_Vector3D::operator *(double& fLambda)
{
	return ACG_Vector3D(fLambda*x, fLambda*y,fLambda*z);
}
void ACG_Vector3D::operator =(const ACG_Vector3D& Vertex)
{
	x = Vertex.x;
	y= Vertex.y;
	z = Vertex.z;
}

ACG_Vector3D ACG_Vector3D::operator /(double& fDivBy)
{
	return ACG_Vector3D(x/fDivBy,y/fDivBy, z/fDivBy);
}

double ACG_Vector3D::dotProduct(ACG_Vector3D& Vertex1, ACG_Vector3D& Vertex2)
{
	return((Vertex1.x*Vertex2.x)+(Vertex1.y*Vertex2.y)+(Vertex1.z*Vertex2.z));
}
ACG_Vector3D ACG_Vector3D::crossProduct(ACG_Vector3D& Vertex1, ACG_Vector3D& Vertex2)
{
	double X_CrossPorduct = ((Vertex1.y*Vertex2.z)-(Vertex2.y*Vertex1.z));
	double Y_CrossProdct = ((Vertex1.z*Vertex2.x - Vertex1.x*Vertex2.z));
	double Z_CrossProduct = ((Vertex1.x*Vertex2.y)-(Vertex2.x*Vertex1.y));

	return ACG_Vector3D(X_CrossPorduct,Y_CrossProdct,Z_CrossProduct);
}

double ACG_Vector3D::length()
{
	return sqrt((x*x)+(y*y)+(z*z));
}
