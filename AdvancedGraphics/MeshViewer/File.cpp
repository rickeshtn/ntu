#include "File.h"

using namespace std;

mFile::mFile()
{

}

mFile::mFile(IN string strFileName)
{
	if (strFileName.size() > 0)
	{
		mStrFileName = strFileName;
	}
	mIsFileOpen = false;
}


bool mFile::openMFile()
{
	bool fileStatus = false;//means not opened yet or couldn't open
	if (!mIsFileOpen )
	{
		mFileSource.open(mStrFileName);

		if (mFileSource.is_open())
		{ 
			fileStatus = true;
			mIsFileOpen = fileStatus;
		}
	}
	return fileStatus;
}
bool mFile::isMfileOpen()
{
	return mIsFileOpen;
}
bool  mFile::closeMfile()
{
	if (mFileSource.is_open())
	{
		mFileSource.close();
		mIsFileOpen = false;
	}
	return mIsFileOpen;
}

string mFile::getFileName()
{
	return mStrFileName;
}
