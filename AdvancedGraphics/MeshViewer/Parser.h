#ifndef PARSER_H
#define PARSER_H

#include "File.h"
#include<vector>
#include <map>
#include "ACG_DataTypes.h"

using namespace std;


class mFileParser : public mFile
{
public:
	mFileParser(IN string FileName);
	~mFileParser();
	bool m_CreateHalfEdgeLists(OUT map<unsigned, ACG_HalfEdge_Vert*>&, OUT vector<ACG_HalfEdgeFace*>&, OUT vector<ACG_HalfEdge_Edge*>&);
private:
	bool m_getAllInfo(OUT map<unsigned, ACG_HalfEdge_Vert*>&, OUT map<unsigned, vector<unsigned>>&);
	bool m_formFacesAndEdges(IN map<unsigned, vector<unsigned>>&, INOUT map<unsigned, ACG_HalfEdge_Vert*>&, OUT vector<ACG_HalfEdgeFace*>&, OUT vector <ACG_HalfEdge_Edge*>&);
};





#endif // PARSER_H
