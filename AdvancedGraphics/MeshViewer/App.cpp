#include <iostream>
#include <algorithm>
#include "App.h"
#include <string>
#include "ACG_Vector3D.h"

using namespace std;

App* App::m_AppInstance = NULL;
App::App()
{
	mBoundingBox = false;
	mTranslation = ACG_Vector3D(0, 0, 0);
	isLoaded = false;
	m_ShowEdges = false;
	m_lightControl = true;
}
App::~App()
{

}
void App::changeShadingType(int Shading)
{
	if (Shading == 1)
	{
		mShadingType = true;
	}
	else
	{
		mShadingType = false;
	}
}
void DrawCylinder()
{
	double baseRadius, TopRadius, Height;
	baseRadius = 0.06;
	TopRadius = baseRadius;
	Height = 3;
	GLint slices, stacks;
	slices = 3;
	stacks = slices;
	GLUquadric* quadObj = gluNewQuadric();
	gluCylinder(quadObj, baseRadius, TopRadius, Height, slices, stacks);
	glPushMatrix();
	glTranslated(0, 0, Height);
	glutSolidCone(baseRadius, 0.1, slices, stacks);
	glPopMatrix();
}
void App::SwitchOnOrOFFLights(int light)
{
	if (light == 1)
		m_lightControl = true;
	else
		m_lightControl = false;
}
void App::EnableLighting()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
}

void App::DisableLighting()
{
	glDisable(GL_NORMALIZE);
	glDisable(GL_LIGHT0);
	glDisable(GL_LIGHTING);
}
void App::initializeRotation()
{
	mRotation[0] = 1.0;
	mRotation[1] = 0.0;
	mRotation[2] = 0.0;
	mRotation[3] = 0.0;
	mRotation[4] = 0.0;
	mRotation[5] = 1.0;
	mRotation[6] = 0.0;
	mRotation[7] = 0.0;
	mRotation[8] = 0.0;
	mRotation[9] = 0.0;
	mRotation[10] = 1.0;
	mRotation[11] = 0.0;
	mRotation[12] = 0.0;
	mRotation[13] = 0.0;
	mRotation[14] = 0.0;
	mRotation[15] = 1.0;
}
bool App::ShowWireFrame()
{
	bool Result = false;
	if (m_VecFaceInfo.size() == 0)
		return Result;

	glTranslated(mTranslation.x, mTranslation.y, mTranslation.z);
	glMultMatrixf(mRotation);
	glScaled(1 / mScaleFactor, 1 / mScaleFactor, 1 / mScaleFactor);
	
	if (m_lightControl)
		EnableLighting();

	if (mShadingType)
		glShadeModel(GL_SMOOTH);
	else
		glShadeModel(GL_FLAT);

	DrawWireFrameEdges();
	//control lightinh with if loop
	if (m_lightControl)
		DisableLighting();

	if (mBoundingBox)
		DrawBoundingBox();

	Result = true;
	return Result;
}

bool App::ShowPolygonalModel()
{
	bool Result = false;

	if (m_VecEdgeInfo.size() == 0)
		return Result;

	glTranslated(mTranslation.x, mTranslation.y, mTranslation.z);
	glMultMatrixf(mRotation);
	glScaled(1 / mScaleFactor, 1 / mScaleFactor, 1 / mScaleFactor);

	if (m_lightControl)
		EnableLighting();

	if (mShadingType)
		glShadeModel(GL_SMOOTH);
	else
		glShadeModel(GL_FLAT);

	glBegin(GL_TRIANGLES);
	vector<ACG_HalfEdgeFace*>::iterator IteratorFace = m_VecFaceInfo.begin();
	for (; IteratorFace != m_VecFaceInfo.end();IteratorFace++)
	{
		ACG_HalfEdge_Edge* InEdge = (*IteratorFace)->m_Edge;
		ACG_HalfEdge_Edge* CurrEdge = InEdge;

		//use face normals if shading type is smooth. 
		if (!mShadingType)
			glNormal3d((*IteratorFace)->m_FaceNormal.x, (*IteratorFace)->m_FaceNormal.y, (*IteratorFace)->m_FaceNormal.z);

		do
		{
			//use vertex normals if shading type is flat. 
			if (mShadingType)
				glNormal3d(CurrEdge->m_EndVert->m_NormalVector.x, CurrEdge->m_EndVert->m_NormalVector.y, CurrEdge->m_EndVert->m_NormalVector.z);

			glVertex3d(CurrEdge->m_EndVert->m_vertInfo.x, CurrEdge->m_EndVert->m_vertInfo.y, CurrEdge->m_EndVert->m_vertInfo.z);
			CurrEdge = CurrEdge->m_NextEdge;
		} while (CurrEdge != InEdge);
	}
	glEnd();
	glFlush();

	//Control Lightings
	if (m_lightControl)
		DisableLighting();
	
	//control Edges with if loop
	if (m_ShowEdges)
	{
		glColor3f(0, 0, 0);
		glLineWidth(2.0f);
		DrawWireFrameEdges();
	}
	if (mBoundingBox)
		DrawBoundingBox();

	Result = true;
	return Result;
}
void App::ShowBoundingBox(bool show)
{
	if (show)
		mBoundingBox = true;
	else
		mBoundingBox = false;
}
void App::DrawAxesOrNot(bool AxesShow)
{
	if (AxesShow)
		mDrawAxes = true;
	else
		mDrawAxes = false;
}

void App::DrawCoordOrNot(bool coordShow)
{
	if (coordShow)
		mDrawAxes = true;
	else
		mDrawAxes = false;
}
void App::ShowEdges( bool show)
{
	if (show)
		m_ShowEdges = true;
	else
		m_ShowEdges = false;
}
void App::DrawBoundingBox()
{
	glColor3f(0, 1.0f, 0);
	glLineWidth(2);
	glBegin(GL_LINES);
	

	glVertex3f(m_MaxVert.x, m_MinVert.y, m_MinVert.z);
	glVertex3f(m_MaxVert.x, m_MaxVert.y, m_MinVert.z);

	glVertex3f(m_MaxVert.x, m_MaxVert.y, m_MinVert.z);
	glVertex3f(m_MinVert.x, m_MaxVert.y, m_MinVert.z);

	glVertex3f(m_MinVert.x, m_MinVert.y, m_MinVert.z);
	glVertex3f(m_MaxVert.x, m_MinVert.y, m_MinVert.z);

	glVertex3f(m_MinVert.x, m_MaxVert.y, m_MinVert.z);
	glVertex3f(m_MinVert.x, m_MinVert.y, m_MinVert.z);


	
	glVertex3f(m_MaxVert.x, m_MinVert.y, m_MaxVert.z);
	glVertex3f(m_MaxVert.x, m_MinVert.y, m_MinVert.z);

	glVertex3f(m_MaxVert.x, m_MinVert.y, m_MaxVert.z);
	glVertex3f(m_MaxVert.x, m_MaxVert.y, m_MaxVert.z);

	glVertex3f(m_MinVert.x, m_MaxVert.y, m_MaxVert.z);
	glVertex3f(m_MinVert.x, m_MinVert.y, m_MaxVert.z);

	glVertex3f(m_MinVert.x, m_MinVert.y, m_MinVert.z);
	glVertex3f(m_MinVert.x, m_MinVert.y, m_MaxVert.z);

	glVertex3f(m_MaxVert.x, m_MaxVert.y, m_MaxVert.z);
	glVertex3f(m_MinVert.x, m_MaxVert.y, m_MaxVert.z);

	glVertex3f(m_MinVert.x, m_MaxVert.y, m_MaxVert.z);
	glVertex3f(m_MinVert.x, m_MaxVert.y, m_MinVert.z);

	glVertex3f(m_MaxVert.x, m_MaxVert.y, m_MaxVert.z);
	glVertex3f(m_MaxVert.x, m_MaxVert.y, m_MinVert.z);

	glVertex3f(m_MinVert.x, m_MinVert.y, m_MaxVert.z);
	glVertex3f(m_MaxVert.x, m_MinVert.y, m_MaxVert.z);

	glEnd();
	glFlush();
}
void App::DrawWireFrameEdges()
{
	vector<ACG_HalfEdgeFace*>::iterator FaceIterator = m_VecFaceInfo.begin();
	for (; FaceIterator != m_VecFaceInfo.end(); FaceIterator++)
	{
		ACG_HalfEdge_Edge* InEdge = (*FaceIterator)->m_Edge;
		ACG_HalfEdge_Edge* CurrEdge = InEdge;

		if (!mShadingType)
			glNormal3d((*FaceIterator)->m_FaceNormal.x, (*FaceIterator)->m_FaceNormal.y, (*FaceIterator)->m_FaceNormal.z);

		glBegin(GL_LINE_LOOP);
		do
		{
			if (mShadingType)
				glNormal3d(CurrEdge->m_EndVert->m_NormalVector.x, CurrEdge->m_EndVert->m_NormalVector.y, CurrEdge->m_EndVert->m_NormalVector.z);

			glVertex3f(CurrEdge->m_EndVert->m_vertInfo.x, CurrEdge->m_EndVert->m_vertInfo.y, CurrEdge->m_EndVert->m_vertInfo.z);
			CurrEdge = CurrEdge->m_NextEdge;
		} while (CurrEdge != InEdge);
		glEnd();
	}
	glFlush();
}

bool App::ShowPointsCloud()
{
	bool Result = false;
	if (m_mapVertInfo.size() == 0)
		return Result;

	glTranslated(mTranslation.x, mTranslation.y, mTranslation.z);
	glMultMatrixf(mRotation);
	glScaled(1 / mScaleFactor, 1 / mScaleFactor, 1 / mScaleFactor);
	if(m_lightControl)
		EnableLighting();

	glPointSize(3.0f);
	glShadeModel(GL_SMOOTH);
	glBegin(GL_POINTS);
	map<unsigned, ACG_HalfEdge_Vert*>::iterator vertexIter;
	for (vertexIter = m_mapVertInfo.begin(); vertexIter != m_mapVertInfo.end(); vertexIter++)
	{
		glNormal3d(vertexIter->second->m_NormalVector.x, vertexIter->second->m_NormalVector.y, vertexIter->second->m_NormalVector.z);
		glVertex3f(vertexIter->second->m_vertInfo.x, vertexIter->second->m_vertInfo.y, vertexIter->second->m_vertInfo.z);
	}
	glEnd();
	glFlush();

	if (m_lightControl)
		DisableLighting();
	if (mBoundingBox)
		DrawBoundingBox();
	Result = true;
	return Result;


}

void  App::DrawAxes()
{
	//X-axis
	glColor3d(1, 0, 0);
	DrawCylinder();
	//Y-Axis
	glPushMatrix();	
	glRotated(90, 0, 1, 0);
	glColor3d(0, 1, 0);
	DrawCylinder();
	glPopMatrix();
	//Z-axis
	glPushMatrix();
	glRotated(-90, 1, 0, 0);
	glColor3d(0, 0, 1);
	DrawCylinder();
	glPopMatrix();
	
}
void App::DrawCoordinateSystem()
{
	GLfloat lineWidth[2];     // Line width range metrics
	GLfloat CurrentSize;
	GLfloat Step;
	int limitMax = 25;
	int limitMin = -25;
	glGetFloatv(GL_LINE_WIDTH_GRANULARITY, &Step);
	glGetFloatv(GL_LINE_WIDTH_RANGE, lineWidth);
	CurrentSize = lineWidth[1];

	for (int i = limitMin; i <= limitMax; i++)
	{
		glBegin(GL_LINES);
		glEnable(GL_LINE_SMOOTH);
		if (i == 0)
		{
			CurrentSize += Step;
			glLineWidth(CurrentSize);
			CurrentSize -= Step;
		}
		else
			glLineWidth(CurrentSize);
		glColor3d(0.0f, 0.0f, 0.0f);

		glVertex3d((double)i, 0.0, -25);
		glVertex3d((double)i, 0.0, 25);

		glEnd();
		glFlush();
	}

	for (int i = limitMin; i <= limitMax; i++)
	{
		glBegin(GL_LINES);
		glEnable(GL_LINE_SMOOTH);
		if (i == 0)
		{
			CurrentSize += Step;
			glLineWidth(CurrentSize);
			CurrentSize -= Step;
		}
		else
			glLineWidth(CurrentSize);
		glColor3d(0.0f, 0.0f, 0.0f);

		glVertex3d(-25, 0.0, double(i));
		glVertex3d(25, 0.0, (double)i);

		glEnd();
		glFlush();
	}
}

bool App::LoadFile(string filename)
{
	//entry point when you load the file
	bool Result = false;
	mFile FileObj;
	if (isLoaded && (filename.compare(FileObj.mStrFileName)==0))
	{
		Result = true;
		return Result;
	}
	if (filename.size() == 0)
	{
		return Result;
	}
	FileParser = new mFileParser(filename);
	if (FileParser != NULL)
	{
		//clearing if previous model was loaded. 
		m_mapVertInfo.clear();
		m_VecFaceInfo.clear();
		m_VecEdgeInfo.clear();
		bool CreateResult = FileParser->m_CreateHalfEdgeLists(m_mapVertInfo, m_VecFaceInfo, m_VecEdgeInfo);

		UpdateFaceNormals();
		UpdateVertexNormals();
		findBoundingBox();
		initializeRotation();
	}
	return Result;
}

App* App::CreateOrGetInstance()
{
	if (App::m_AppInstance == NULL)
	{
		App::m_AppInstance = new App();
		return App::m_AppInstance;
	}
	else
		return App::m_AppInstance;
}
bool App::findBoundingBox()
{
	bool Result = false;
	vector<ACG_HalfEdge_Vert*> vecVert;
	vector<double> vecMaxVal;
	map<unsigned, ACG_HalfEdge_Vert*>::iterator vertIter = m_mapVertInfo.begin();
	for (; vertIter != m_mapVertInfo.end(); vertIter++)
	{
		vecVert.push_back(vertIter->second);
	}
	sort(vecVert.begin(), vecVert.end(), ACG_HalfEdge_Vert::greaterByX);
	m_MaxVert.x = vecVert.front()->m_vertInfo.x;
	m_MinVert.x = vecVert.back()->m_vertInfo.x;
	vecMaxVal.push_back(m_MaxVert.x);

	sort(vecVert.begin(), vecVert.end(), ACG_HalfEdge_Vert::greaterByY);
	m_MaxVert.y = vecVert.front()->m_vertInfo.y;
	m_MinVert.y = vecVert.back()->m_vertInfo.y;
	vecMaxVal.push_back(m_MaxVert.y);

	sort(vecVert.begin(), vecVert.end(), ACG_HalfEdge_Vert::greaterByZ);
	m_MaxVert.z = vecVert.front()->m_vertInfo.z;
	m_MinVert.z = vecVert.back()->m_vertInfo.z;
	vecMaxVal.push_back(m_MaxVert.z);

	sort(vecMaxVal.begin(), vecMaxVal.end());
	mScaleFactor = vecMaxVal.back();
		
	Result = true;
	return Result;
}

double App::CalculateArea(unsigned VeretexID)
{
	ACG_HalfEdge_Edge *pInitEdge = m_mapVertInfo[VeretexID]->m_HalfEdge;
	ACG_HalfEdge_Edge *pCurrEdge = pInitEdge;

	double dTotalArea = 0.0;

	bool isBoundary = true;
	while (pCurrEdge->m_PairEdge != NULL)
	{
		if (pCurrEdge->m_PairEdge->m_NextEdge == pInitEdge)
		{
			isBoundary = false;
			break;
		}
		dTotalArea += pCurrEdge->m_IncidentFace->m_Area;

		pCurrEdge = pCurrEdge->m_PairEdge->m_NextEdge;
	}
	if (isBoundary)
	{
		pCurrEdge = pInitEdge;

		while (pCurrEdge->m_PrevEdge->m_PairEdge != NULL)
		{
			if (pCurrEdge->m_PrevEdge->m_PairEdge == pInitEdge)
			{
				break;
			}

			pCurrEdge = pCurrEdge->m_PrevEdge->m_PairEdge;
			dTotalArea += pCurrEdge->m_IncidentFace->m_Area;
		}
	}

	return dTotalArea;

}
bool App::UpdateFaceNormals()
{
	bool Result = false;
	vector<ACG_HalfEdgeFace*>::iterator faceIterator = m_VecFaceInfo.begin();
	for (; faceIterator != m_VecFaceInfo.end(); faceIterator++)
	{
		ACG_Vector3D firstEdge = ((*faceIterator)->m_Edge->m_EndVert->m_vertInfo - (*faceIterator)->m_Edge->m_PrevEdge->m_EndVert->m_vertInfo);
		ACG_Vector3D secondEdge = ((*faceIterator)->m_Edge->m_NextEdge->m_EndVert->m_vertInfo - (*faceIterator)->m_Edge->m_EndVert->m_vertInfo);

		ACG_Vector3D CrossProduct = ACG_Vector3D::crossProduct(firstEdge, secondEdge);
		double length = CrossProduct.length();
		(*faceIterator)->m_FaceNormal = CrossProduct;
		(*faceIterator)->m_Area = 0.5f*length;
	}
	return Result;
}


bool App::UpdateVertexNormals()
{
	bool Result = false;
	map<unsigned, ACG_HalfEdge_Vert*>::iterator vertexIter = m_mapVertInfo.begin();
	for (; vertexIter != m_mapVertInfo.end(); vertexIter++)
	{
		ACG_Vector3D VertNormal;
		double dTotalArea = App::CalculateArea(vertexIter->first);
		//double dTotalArea = 0.0;
		ACG_HalfEdge_Edge* pInitEdge = (*vertexIter).second->m_HalfEdge;
		ACG_HalfEdge_Edge* pCurrentEdge = pInitEdge;
		bool isBoundary = true;
		while (pCurrentEdge->m_PairEdge != NULL)
		{
			if (pCurrentEdge->m_PairEdge->m_NextEdge == pInitEdge)
			{
				isBoundary = false;
				break;
			}
			double dWeight = (pCurrentEdge->m_IncidentFace->m_Area / dTotalArea);
			VertNormal = (VertNormal + (pCurrentEdge->m_IncidentFace->m_FaceNormal * dWeight));
			dTotalArea += pCurrentEdge->m_IncidentFace->m_Area;
			pCurrentEdge = pCurrentEdge->m_PairEdge->m_NextEdge;
		}
		if (isBoundary)
		{
			pCurrentEdge = pInitEdge;
			while (pCurrentEdge->m_PrevEdge->m_PairEdge != NULL)
			{
				if (pCurrentEdge->m_PrevEdge->m_PairEdge == pInitEdge)
				{
					break;
				}
				pCurrentEdge = pCurrentEdge->m_PrevEdge->m_PairEdge;
				dTotalArea += pCurrentEdge->m_IncidentFace->m_Area;
				double dWeight = (pCurrentEdge->m_IncidentFace->m_Area / dTotalArea);
				VertNormal = (VertNormal + (pCurrentEdge->m_IncidentFace->m_FaceNormal * dWeight));
			}
		}
		double length = VertNormal.length();
		(*vertexIter).second->m_NormalVector = VertNormal / length;
	}

	return Result;
}
