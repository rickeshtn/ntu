#include "Parser.h"
#include <iostream>
#include <glut.h>
#include <gl\GL.h>
#include <string>
#include <sstream>
#include "App.h"
#include "File.h"
using namespace std;

mFileParser::mFileParser(IN string FileName) : mFile(FileName)
{

}

mFileParser :: ~mFileParser()
{

}


bool mFileParser::m_getAllInfo(OUT map<unsigned, ACG_HalfEdge_Vert*>& mapVertexInfo, 
								OUT map<unsigned, vector<unsigned>>& mapFaceInfo)
{
	bool status = false;
	if (isMfileOpen())
	{
		string mLine = "";
		string header = "";
		string delimiter = " ";
		while (getline(mFileSource, mLine))
		{
			istringstream line(mLine);
			line >> header;

			if (header == "Vertex")
			{
				int VerteID;
				line >> VerteID;
				ACG_HalfEdge_Vert *Vertex = new ACG_HalfEdge_Vert();
				Vertex->m_vertId = VerteID;
				//line >> Vertex->m_vertId;
				line >> Vertex->m_vertInfo.x;
				line >> Vertex->m_vertInfo.y;
				line >> Vertex->m_vertInfo.z;
				mapVertexInfo[VerteID] = Vertex;
				//line >>Vertex->m_vertId >> Vertex->m_vertInfo.x>>Vertex->m_vertInfo.y>>Vertex->m_vertInfo.z;
			}
			else if (header == "Face")
			{
				ACG_HalfEdgeFace * Face = new ACG_HalfEdgeFace();
				line >> Face->m_FaceId;
				int VertexID;
				while (line >> VertexID)
				{
					mapFaceInfo[Face->m_FaceId].push_back(VertexID);
				}
				if (mapFaceInfo[Face->m_FaceId].size() < 3)
					mapFaceInfo.erase(Face->m_FaceId);			
			}
		}
		status = true;
	}
	return status;
}


bool  mFileParser::m_formFacesAndEdges(IN map<unsigned, vector<unsigned>>& mapFaceDetails,  
										INOUT map<unsigned, ACG_HalfEdge_Vert*>& mapVertDetails, 
											OUT vector<ACG_HalfEdgeFace*>& VectorFaceDetails, 
												OUT vector <ACG_HalfEdge_Edge*>& vectorEdgeDetails )
{
	bool result = false;
	//formed edges.. 
	map<pair<unsigned, unsigned>, ACG_HalfEdge_Edge*> mapEdgeFormed;
	//write the iterator iterating over the loaded edges from the previus function
	map<unsigned, vector<unsigned>>::iterator faceIterator = mapFaceDetails.begin();

	for (; faceIterator != mapFaceDetails.end(); faceIterator++)
	{
		ACG_HalfEdgeFace* mFace = new ACG_HalfEdgeFace();
		mFace->m_FaceId = faceIterator->first;
		VectorFaceDetails.push_back(mFace); //push the face id and go ahead to find other stuffss. 
		vector<unsigned>::iterator VertexsOfFaceIterator = (*faceIterator).second.begin();
		int FirstVertex = (*VertexsOfFaceIterator);
		ACG_HalfEdge_Edge* mPrevEdge = NULL;
		ACG_HalfEdge_Edge* mFirstEdge = NULL;
		for (; VertexsOfFaceIterator != (*faceIterator).second.end(); VertexsOfFaceIterator++)
		{
			int CurrentVertex = (*VertexsOfFaceIterator);
			int NextVertex = FirstVertex;
			bool isLastEdge = false;
			VertexsOfFaceIterator++;

			if (VertexsOfFaceIterator != (*faceIterator).second.end())
				NextVertex = (*VertexsOfFaceIterator);
			else
				isLastEdge = true;


			ACG_HalfEdge_Edge *mEdge = new ACG_HalfEdge_Edge();
			mEdge->m_IncidentFace = mFace;
			//
			if (mPrevEdge == NULL)
			{
				mFirstEdge = mEdge;
				mFace->m_Edge = mEdge;
			}
			else
			{
				mPrevEdge->m_NextEdge = mEdge;
				mEdge->m_PrevEdge = mPrevEdge;
			}

			if (mapVertDetails[CurrentVertex]->m_HalfEdge == NULL)
			{
				mapVertDetails[CurrentVertex]->m_HalfEdge = mEdge;
			}

			if (isLastEdge)
			{
				mEdge->m_EndVert = mapVertDetails[FirstVertex];
				mEdge->m_NextEdge = mFirstEdge;
				mFirstEdge->m_PrevEdge = mEdge;
			}
			else
			{
				mEdge->m_EndVert = mapVertDetails[NextVertex];
			}

			mPrevEdge = mEdge;
			mapEdgeFormed[make_pair(CurrentVertex, NextVertex)] = mEdge;
			if (mapEdgeFormed.find(make_pair(NextVertex, CurrentVertex)) != mapEdgeFormed.end())
			{
				mapEdgeFormed[make_pair(NextVertex, CurrentVertex)]->m_PairEdge = mEdge;
				mEdge->m_PairEdge = mapEdgeFormed[make_pair(NextVertex,CurrentVertex)];
			}
			VertexsOfFaceIterator--;
		}
	}
	map<pair<unsigned, unsigned>, ACG_HalfEdge_Edge*>::iterator IterEdge = mapEdgeFormed.begin();
	for (; IterEdge != mapEdgeFormed.end(); IterEdge++)
	{
		vectorEdgeDetails.push_back(IterEdge->second);
	}

	return result;
}

bool mFileParser::m_CreateHalfEdgeLists(OUT map<unsigned, ACG_HalfEdge_Vert*>& mapVertinfo, 
											OUT vector<ACG_HalfEdgeFace*>& vectorFaceInfo, 
												OUT vector<ACG_HalfEdge_Edge*>& vectorEdgeInfo)
{
	bool result = false;
	map<unsigned, vector<unsigned>> mReadFaceDetails;
	result = mFile::openMFile();
	if (result == false)
	{
		return result;
	}
	else
	{
		//Call vertex and face iterations in another function
		result = m_getAllInfo(mapVertinfo, mReadFaceDetails);
		//call face based iteration
		result = m_formFacesAndEdges(mReadFaceDetails, mapVertinfo, vectorFaceInfo,vectorEdgeInfo);
		cout << "Number of Faces: " << vectorFaceInfo.size() << endl;
		cout << "Number of Edges: " << vectorEdgeInfo.size() << endl;
		//We are done loading hence close the file now.
		closeMfile();
	}

	return result;
}
