#ifndef App_H
#define App_H

#include <glut.h>
#include <gl\GL.h>
#include "Parser.h"
#include "ACG_DataTypes.h"
using namespace std;

class App
{
public:
	bool LoadFile(std::string filename);
	static App* CreateOrGetInstance();
	void DrawCoordinateSystem();
	bool UpdateVertexNormals();
	bool UpdateFaceNormals();
	double CalculateArea(unsigned VertexID);
	App();
	~App();
	void DrawAxes();
	bool ShowPointsCloud();
	bool ShowWireFrame();
	bool ShowPolygonalModel();
	void ShowBoundingBox(bool show);
	void DrawCoordOrNot(bool coordShow);
	void DrawAxesOrNot(bool AxesShow);
	void ShowEdges(bool show);
	ACG_Vector3D mTranslation;
	GLfloat mRotation[16];
	void EnableLighting();
	void DisableLighting();
	void SwitchOnOrOFFLights(int light);
	void initializeRotation();
	bool findBoundingBox();
	void changeShadingType(int Shading);
	void DrawWireFrameEdges();
	void DrawBoundingBox();

private:
	static App* m_AppInstance;
	bool mBoundingBox;
	bool mDrawAxes;
	bool mDrawCoord;
	bool isLoaded;
	bool isSmooth;
	double mScaleFactor;
	bool m_ShowEdges;
	bool m_lightControl;
	bool LightsOnOrOFF;
	bool mShadingType;
	mFileParser* FileParser;
	map<unsigned, ACG_HalfEdge_Vert*> m_mapVertInfo;
	vector<ACG_HalfEdgeFace*> m_VecFaceInfo;
	vector<ACG_HalfEdge_Edge*> m_VecEdgeInfo;
	ACG_Vector3D m_VertInfo;
	ACG_Vector3D m_MaxVert;
	ACG_Vector3D m_MinVert;
	App(const App&);
};

#endif