// MeshViewer.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <string>
#include "App.h"
#include <glut.h>
#include <gl\GL.h>
#include <glui.h>


#pragma comment( linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"" )

using namespace std;
GLUI_FileBrowser *BrowseFiles;
bool isFileLoaded(false);
int last_x, last_y;
float xy_aspect;
ACG_Vector3D lookAt(0, 0, 0);
ACG_Vector3D eyeXZ(0.000000001, 5, 0);
ACG_Vector3D eyeYZ(5, 0, 0);
ACG_Vector3D eyeXY(0, 0, 5);
ACG_Vector3D eye3D(5, 3, 5);
ACG_Vector3D Eye(5, 3, 5);
int currButton = -1;
int lastPosX = 0;
int lastPosY = 0;
int cameraType = 0;
int renderingType = 2;
int ShadingType = 1;
int BoundingBoxOptions = 0;
int DrawCoordOrNot = 1;
int DrawAxesorNot = 1;
int ShowEdges = 0;
int LightsOnOff = 1;
int lightSelect = 0;
int ProjectionType = 0; int projType = 0;
int windowWidth = 1300;
int windowHeight = 650;
GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 }; // light position
GLfloat white_light[] = { 1.0, 1.0, 1.0, 1.0 }; // light color
GLfloat lmodel_ambient[] = { 1, 0.1, 0.1, 1.0 };//ambient light
GLfloat specular_light[] = { 0.2, 0.2, 0.2, 1.0 };//specular light
GLUI_Spinner *RedColor, *GreenColor, *BlueColor;//light colors
GLdouble fovy = 65,aspect =1 ,zNear = 0.001,zfar = 100;//toggle perspective projection values
//GLdouble Oleft = -1, Oright = 1, Obottom= -1, OTop= 1,OzNear=5,OzFar=100;
GLdouble Oleft = -5, Oright = 5, Obottom = -5, OTop = 5, OzNear = 0.001, OzFar = 100;
GLUI_Spinner *fovySp, *AspectSp, *NearSp, *FarSp;
GLUI_Spinner *OleftSp, *OrightSp, *ObottomSp, *OTopSp, *OzNearSp, *OzFarSp;
GLfloat objectPosition[] = { 0.0, 0.0, 0.0 };
GLfloat objectRotation[] = { 1.0, 0.0, 0.0, 0.0,
							  0.0, 1.0, 0.0, 0.0,
							  0.0, 0.0, 1.0, 0.0,
							  0.0, 0.0, 0.0, 1.0 };

void Reset(int optiongrp)
{
	fovy = 65; aspect = 1; zNear = 1.5; zfar = 10;
	//Oleft = -1; Oright = 1; Obottom = -1; OTop = 1; OzNear = 5; OzFar = 100;
	Oleft = -5; Oright = 5; Obottom = -5; OTop = 5; OzNear = 0.001; OzFar = 100;
	lookAt.x = 0; lookAt.y = 0; lookAt.y=0;
	eyeXZ.x = 0.000000001; eyeXZ.y = 5, eyeXZ.z = 0;
	eyeYZ.x = 5; eyeYZ.y = 0; eyeYZ.z = 0;
	eyeXY.x = 0; eyeXY.y = 0; eyeXY.z = 5;
	eye3D.x = 5; eye3D.y = 3; eye3D.z = 5;
	Eye.x = 5; Eye.y = 3; Eye.z = 5;
	
	/*currButton = -1;
	lastPosX = 0;
	lastPosY = 0;
	cameraType = 0;
	renderingType = 2;
	ShadingType = 1;
	BoundingBoxOptions = 0;
	DrawCoordOrNot = 1;
	DrawAxesorNot = 1;
	ShowEdges = 0;
	LightsOnOff = 1;
	lightSelect = 0;
	ProjectionType = 0; projType = 0;*/
	/*windowWidth = 970;
	windowHeight = 650;*/
	light_position[0] = 1.0; light_position[1] = 1.0; light_position[2] = 1.0; light_position[4] = 0.0;
	white_light[4] = { 1.0}; // light color
	lmodel_ambient[0] = 1; lmodel_ambient[1] = 0.1; lmodel_ambient[2] = 0.1; lmodel_ambient[3] = 1.0;
	specular_light[0] = 0.2; specular_light[1] = 0.2; specular_light[2] = 0.2; specular_light[3] = 1.0;
}
void initRendering()
{
	glClearColor(1, 1, 1, 1);
	glEnable(GL_DEPTH_TEST);
}
void ControlShadingType(int optionGroup)
{
	App::CreateOrGetInstance()->changeShadingType(ShadingType);
}

void light_callback(int ID) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glutPostRedisplay();
}
void TransLateAndRotOptions(int i)
{
	if (!isFileLoaded)
		return;
	else
	{
		switch (i)
		{
		case 1: 
			App::CreateOrGetInstance()->mTranslation.x = objectPosition[0];
			break;
		case 2:
			App::CreateOrGetInstance()->mTranslation.y = objectPosition[1];
			break;
		case 3:
			App::CreateOrGetInstance()->mTranslation.z = objectPosition[2];
			break;
		case 4:
			App::CreateOrGetInstance()->mRotation[0] = objectRotation[0];
			App::CreateOrGetInstance()->mRotation[1] = objectRotation[1];
			App::CreateOrGetInstance()->mRotation[2] = objectRotation[2];
			App::CreateOrGetInstance()->mRotation[3] = objectRotation[3];
			App::CreateOrGetInstance()->mRotation[4] = objectRotation[4];
			App::CreateOrGetInstance()->mRotation[5] = objectRotation[5];
			App::CreateOrGetInstance()->mRotation[6] = objectRotation[6];
			App::CreateOrGetInstance()->mRotation[7] = objectRotation[7];
			App::CreateOrGetInstance()->mRotation[8] = objectRotation[8];
			App::CreateOrGetInstance()->mRotation[9] = objectRotation[9];
			App::CreateOrGetInstance()->mRotation[10] = objectRotation[10];
			App::CreateOrGetInstance()->mRotation[11] = objectRotation[11];
			App::CreateOrGetInstance()->mRotation[12] = objectRotation[12];
			App::CreateOrGetInstance()->mRotation[13] = objectRotation[13];
			App::CreateOrGetInstance()->mRotation[14] = objectRotation[14];
			App::CreateOrGetInstance()->mRotation[15] = objectRotation[15];
			break;
		}
	}
}
void lightSelection(int optionGroup)
{
	switch (lightSelect)
	{
	case 0:
		RedColor->set_float_val(lmodel_ambient[0]);
		GreenColor->set_float_val(lmodel_ambient[1]);
		BlueColor->set_float_val(lmodel_ambient[2]);
		break;
	case 1:
		RedColor->set_float_val(white_light[0]);
		GreenColor->set_float_val(white_light[1]);
		BlueColor->set_float_val(white_light[2]);
		break;
	case 2:
		RedColor->set_float_val(specular_light[0]);
		GreenColor->set_float_val(specular_light[1]);
		BlueColor->set_float_val(specular_light[2]);
		break;
	default:
		break;
	}
}
void ProjectionPerspective(int optionGrp)
{
	switch (optionGrp)
	{
	case 1:
		fovy = fovySp->get_float_val();
	case 2:
		aspect = AspectSp->get_float_val();
	case 3:
		zNear = NearSp->get_float_val();
	case 4:
		zfar = FarSp->get_float_val();
	}
}

void OrthoProjection(int optionGrp)
{
	switch (optionGrp)
	{
	case 1:
		Oleft= OleftSp->get_float_val();
	case 2:
		 Oright= OrightSp->get_float_val();
	case 3:
		Obottom = ObottomSp->get_float_val();
	case 4:
		OTop= OTopSp->get_float_val();
	case 5:
		OzNear = OzNearSp->get_float_val();
	case 6:
		OzFar = OzFarSp->get_float_val();
	}
}
void lightingOptions(int optionGroup)
{
	switch (optionGroup)
	{
	case 1:
		App::CreateOrGetInstance()->SwitchOnOrOFFLights(LightsOnOff);
	case 2:
		if (lightSelect == 0)
			lmodel_ambient[0] = RedColor->get_float_val();
		else if (lightSelect == 1)
			white_light[0] = RedColor->get_float_val();
		else if (lightSelect == 2)
			specular_light[0] = RedColor->get_float_val();
		break;
	case 3:
		if (lightSelect == 0)
			lmodel_ambient[1] = GreenColor->get_float_val();
		else if (lightSelect == 1)
			white_light[1] = GreenColor->get_float_val();
		else if (lightSelect == 2)
			specular_light[1] = GreenColor->get_float_val();
		break;
	case 4:
		if (lightSelect == 0)
			lmodel_ambient[2] = BlueColor->get_float_val();
		else if (lightSelect == 1)
			white_light[2] = BlueColor->get_float_val();
		else if (lightSelect == 2)
			specular_light[2] = BlueColor->get_float_val();
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void Mouse(int button, int button_state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && button_state == GLUT_DOWN) {
		last_x = x;
		last_y = y;
	}
}
void Projection(int i)
{
	if (ProjectionType == 1)
	{
		projType = 1;
	}
	else if (ProjectionType == 2)
	{
		projType = 2;
		/*glMatrixMode(GL_PROJECTION);
		glLoadIdentity();*/

	}
	else
		projType = 0;
}
void cameraControl(int i)
{
	switch (cameraType)
	{
	case CameraTypes::CAMERA_3D:
			Eye = eye3D;
			break;
	case CameraTypes::CAMERA_XZ:
		Eye = eyeXZ;
		break;
	case CameraTypes::CAMERA_YZ:
		Eye = eyeYZ;
		break;
	case CameraTypes::CAMERA_XY:
		Eye = eyeXY;
		break;
	default:
		break;
	}
}
void MouseFunc(int x, int y)
{
	int dx = x - lastPosX;
	int dy = y - lastPosY;

	double scale, length, theta;
	ACG_Vector3D cNEye1;
	ACG_Vector3D cNEye2;
	ACG_Vector3D cFar;
	ACG_Vector3D cIdent;
	ACG_Vector3D cRes;

	switch (currButton)
	{
	case GLUT_MIDDLE_BUTTON:
	{
		cFar = lookAt - Eye;
		cIdent = ACG_Vector3D(0, 1, 0);

		scale = sqrt(cFar.length()) * 0.003;

		cRes = ACG_Vector3D::crossProduct(cFar, cIdent);
		cIdent = ACG_Vector3D::crossProduct(cRes, cFar);

		double mag = cRes.length();
		cRes = cRes / mag;

		mag = cIdent.length();
		cIdent = cIdent / mag;

		Eye.x += -cRes.x * dx * scale + cIdent.x * dy * scale;
		Eye.y += -cRes.y * dx * scale + cIdent.y * dy * scale;
		Eye.z += -cRes.z * dx * scale + cIdent.z * dy * scale;

		lookAt.x += -cRes.x * dx * scale + cIdent.x * dy * scale;
		lookAt.y += -cRes.y * dx * scale + cIdent.y * dy * scale;
		lookAt.z += -cRes.z * dx * scale + cIdent.z * dy * scale;

		break;
	}
	case GLUT_RIGHT_BUTTON:
	{
		cFar = lookAt - Eye;

		length = cFar.length();
		cFar = cFar / length;

		length -= sqrt(length)*dx*0.03;

		Eye.x = lookAt.x - length * cFar.x;
		Eye.y = lookAt.y - length * cFar.y;
		Eye.z = lookAt.z - length * cFar.z;

		if (length < 1)
		{
			lookAt = Eye + cFar;
		}

		break;
	}
	case GLUT_LEFT_BUTTON:
	{
		if (cameraType != CameraTypes::CAMERA_3D)
			break;

		cNEye1 = Eye - lookAt;

		theta = -dx * 0.003;

		cNEye2.x = (double)cos(theta) * cNEye1.x + (double)sin(theta) * cNEye1.z;
		cNEye2.y = cNEye1.y;
		cNEye2.z = -(double)sin(theta) * cNEye1.x + (double)cos(theta) * cNEye1.z;

		theta = -dy * 0.003;

		cFar.x = -cNEye2.x;
		cFar.y = -cNEye2.y;
		cFar.z = -cNEye2.z;
		cIdent = ACG_Vector3D(0, 1, 0);

		cRes = ACG_Vector3D::crossProduct(cFar, cIdent);
		cIdent = ACG_Vector3D::crossProduct(cRes, cFar);

		length = cFar.length();
		cFar = cFar / length;

		double mag = cIdent.length();
		cIdent = cIdent / mag;

		cNEye1.x = length * ((double)cos(theta) * cFar.x + (double)sin(theta) * cIdent.x);
		cNEye1.y = length * ((double)cos(theta) * cFar.y + (double)sin(theta) * cIdent.y);
		cNEye1.z = length * ((double)cos(theta) * cFar.z + (double)sin(theta) * cIdent.z);

		Eye = lookAt - cNEye1;

		break;
	}
	}

	lastPosX = x;
	lastPosY = y;

	glutPostRedisplay();
}

void Exit(int i)
{
	exit(0);
}
void Display()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (projType == 1)
	{
		gluPerspective(fovy, ((GLdouble)windowWidth / (GLdouble)windowHeight)*aspect, zNear, zfar);
	}
	else if (projType == 2)
	{
		//glOrtho(5, 5, 5, 5, 0.001, 100);
		//glOrtho(0.0f, windowWidth, windowHeight, 0.0f, 0.0f, 1.0f);
		glOrtho(Oleft, Oright, Obottom, OTop, OzNear, OzFar);
	}
	else
	{
		cout << "Nothing " << endl;
		gluPerspective(60, ((GLdouble)windowWidth / (GLdouble)windowHeight), 1.0, 100.0);
	}
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(Eye.x, Eye.y, Eye.z, lookAt.x, lookAt.y, lookAt.z, 0, 1.0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLineWidth(0.5f);
	if (DrawCoordOrNot==1)
		App::CreateOrGetInstance()->DrawCoordinateSystem();
	if (DrawAxesorNot ==1)
		App::CreateOrGetInstance()->DrawAxes();

	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, white_light);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular_light);
	glMateriali(GL_FRONT, GL_SHININESS, 100);
	

	if (isFileLoaded)
	{
		if (renderingType == 2)
			App::CreateOrGetInstance()->ShowPointsCloud();
		else if (renderingType == 1)
		{
			//Call wireframe
			App::CreateOrGetInstance()->ShowWireFrame();
			cout << "Wireframe call " << endl;
		}
		else if (renderingType == 0)
		{
			App::CreateOrGetInstance()->ShowPolygonalModel();
			cout << "Fill calle" << endl;
			//call fill
		}
	}

	glutSwapBuffers();
}

void Reshape(int x, int y)
{
	GLUI_Master.auto_set_viewport();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, ((GLdouble)x / (GLdouble)y), 1.0, 100.0);
}
void ShowBoundingBox(int option)
{
	if (option == 1)
		App::CreateOrGetInstance()->ShowBoundingBox((bool)BoundingBoxOptions);
	else if (option == 2)
		App::CreateOrGetInstance()->ShowBoundingBox((bool)BoundingBoxOptions);
}
void ShowCoordOrNot(int option)
{
	if (option == 1)
		App::CreateOrGetInstance()->DrawCoordOrNot(true);
	else
		App::CreateOrGetInstance()->DrawCoordOrNot(false);
}

void ShowAxesOrNot(int option)
{
	if (option == 1)
		App::CreateOrGetInstance()->DrawAxesOrNot(true);
	else
		App::CreateOrGetInstance()->DrawAxesOrNot(false);
}

void EdgesShowOrNot(int option)
{
	if (ShowEdges == 0)
		App::CreateOrGetInstance()->ShowEdges(false);
	else
		App::CreateOrGetInstance()->ShowEdges(true);

}
void openFile(int i)
{
	mFile fileObj;
	string Filename = BrowseFiles->get_file();
	string delimiter = ".";
	string CheckForMFile = Filename.substr(Filename.find(delimiter), Filename.size());
	

	if (CheckForMFile == ".m")
	{
		isFileLoaded = true;
		fileObj.mStrFileName = Filename;
		App* AppInstance = App::CreateOrGetInstance();
		AppInstance->LoadFile(Filename);
		glutPostRedisplay();
	}
	else
	{
		isFileLoaded = false;
		return;
	}

}

void mouseButton(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN)
	{
		currButton = button;
	}
	else
	{
		if (button == currButton)
		{
			currButton = -1;
		}
	}
	lastPosX = x;
	lastPosY = y;
}


int main()
{
	//Initialize GLUT
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth,windowHeight);

	//Create Window
	int glutWindow = glutCreateWindow("MeshViewer");
	initRendering();

	GLUI *gluiWindow = GLUI_Master.create_glui_subwindow(glutWindow, GLUI_SUBWINDOW_RIGHT);
	GLUI_Master.set_glutDisplayFunc(Display);
	GLUI_Master.set_glutReshapeFunc(Reshape);
	GLUI_Master.set_glutMouseFunc(mouseButton);
	glutMotionFunc(MouseFunc);
	GLUI *LeftWindow = GLUI_Master.create_glui_subwindow(glutWindow, GLUI_SUBWINDOW_LEFT);
	GLUI_Panel* filePanel = gluiWindow->add_panel("Browse Files");//create a window to browse files
	BrowseFiles = new GLUI_FileBrowser(filePanel, "", false, 1, openFile); //Call the file browser and extension function
	
	GLUI_Panel *optionsPanel = gluiWindow->add_panel("Object Display");

	GLUI_Panel *renderPanel = gluiWindow->add_panel_to_panel(optionsPanel, "Render Type");
	GLUI_RadioGroup *renderType = gluiWindow->add_radiogroup_to_panel(renderPanel, &renderingType);
	GLUI_RadioButton *fill = gluiWindow->add_radiobutton_to_group(renderType, "Fill");
	GLUI_RadioButton *wireframe = gluiWindow->add_radiobutton_to_group(renderType, "Wireframe");
	GLUI_RadioButton *points = gluiWindow->add_radiobutton_to_group(renderType, "Points");
	
	gluiWindow->add_separator_to_panel(optionsPanel);
	GLUI_Panel *shadeingPanel = gluiWindow->add_panel_to_panel(optionsPanel, "Shading Type");
	GLUI_RadioGroup *PanelShadingType = gluiWindow->add_radiogroup_to_panel(shadeingPanel, &ShadingType, 2, ControlShadingType);
	GLUI_RadioButton *Flat = gluiWindow->add_radiobutton_to_group(PanelShadingType, "FLAT");
	GLUI_RadioButton *Smooth = gluiWindow->add_radiobutton_to_group(PanelShadingType, "SMOOTH");
	
	GLUI_Panel *DisplayPanel = gluiWindow->add_panel_to_panel(optionsPanel,"Display Options",1);
	GLUI_Checkbox *BoundingBox = new GLUI_Checkbox(DisplayPanel, "Bounding Box", &BoundingBoxOptions, 2, ShowBoundingBox);//Toggle boundingBoxx
	GLUI_Checkbox *CoordniateSystem = new GLUI_Checkbox(DisplayPanel, "Ground ", &DrawCoordOrNot, 2, ShowCoordOrNot);//Toggle ground
	GLUI_Checkbox *CoordniateAxes = new GLUI_Checkbox(DisplayPanel, "Axes", &DrawAxesorNot, 2, ShowAxesOrNot);
	GLUI_Checkbox *DrawEdges = new GLUI_Checkbox(DisplayPanel, "Draw Edges", &ShowEdges, 2,EdgesShowOrNot);
	

	//adding camera controls
	GLUI_Panel *cameraPanel = gluiWindow->add_panel("Camera Control");
	GLUI_RadioGroup *cameraControlRadio = gluiWindow->add_radiogroup_to_panel(cameraPanel, &cameraType, 1, cameraControl);
	GLUI_RadioButton *Perspective = gluiWindow->add_radiobutton_to_group(cameraControlRadio, "Perspective View");
	GLUI_RadioButton *XY = gluiWindow->add_radiobutton_to_group(cameraControlRadio, "Top View");
	GLUI_RadioButton *YZ = gluiWindow->add_radiobutton_to_group(cameraControlRadio, "Side View");
	GLUI_RadioButton *Zx = gluiWindow->add_radiobutton_to_group(cameraControlRadio, "Front View");
	gluiWindow->add_separator();

	// light options 
	GLUI_Panel* lightPanel = LeftWindow->add_panel("Light options");
	
	//call in display after writing as a functiom
	/*for (unsigned int i = 0; i<4; ++i)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,light_position[i] );*/
	GLUI_Rollout *light_position_rollout = new GLUI_Rollout(lightPanel, "Light Positions", true);
	GLUI_Spinner *x_light = new GLUI_Spinner(light_position_rollout, "X", GLUI_SPINNER_FLOAT, &(light_position[0]), 1, light_callback);
	x_light->set_float_limits(-50.0, 50.0, GLUI_LIMIT_CLAMP);
	GLUI_Spinner *y_light = new GLUI_Spinner(light_position_rollout, "Y", GLUI_SPINNER_FLOAT, &(light_position[1]), 2, light_callback);
	y_light->set_float_limits(-50.0, 50.0, GLUI_LIMIT_CLAMP);
	GLUI_Spinner *z_light = new GLUI_Spinner(light_position_rollout, "Z", GLUI_SPINNER_FLOAT, &(light_position[2]), 3, light_callback);
	z_light->set_float_limits(-50.0, 50.0, GLUI_LIMIT_CLAMP);
	LeftWindow->add_separator_to_panel(lightPanel);
	//Toggle lighting with different colors
	GLUI_Checkbox* LightOnOff = new GLUI_Checkbox(lightPanel,"Lights ON/OFF",&LightsOnOff,1,lightingOptions);
	GLUI_RadioGroup *lightGrp = LeftWindow->add_radiogroup_to_panel(lightPanel, &lightSelect, 1, lightSelection);
	GLUI_RadioButton *ambience = LeftWindow->add_radiobutton_to_group(lightGrp, "Ambience ");
	GLUI_RadioButton *diffuse = LeftWindow->add_radiobutton_to_group(lightGrp, "Diffuse ");
	GLUI_RadioButton *specular = LeftWindow->add_radiobutton_to_group(lightGrp, "Specular");
	LeftWindow->add_separator_to_panel(lightPanel);
	RedColor = new GLUI_Spinner(lightPanel, "Red", GLUI_SPINNER_FLOAT, 2, lightingOptions);
	RedColor->set_float_limits(0, 1, GLUI_LIMIT_CLAMP);
	RedColor->set_float_val(lmodel_ambient[0]);
	GreenColor = new GLUI_Spinner(lightPanel, "Green", GLUI_SPINNER_FLOAT, 3, lightingOptions);
	GreenColor->set_float_limits(0, 1, GLUI_LIMIT_CLAMP);
	GreenColor->set_float_val(lmodel_ambient[1]);
	BlueColor = new GLUI_Spinner(lightPanel, "Blue", GLUI_SPINNER_FLOAT, 4, lightingOptions);
	BlueColor->set_float_limits(0, 1, GLUI_LIMIT_CLAMP);
	BlueColor->set_float_val(lmodel_ambient[2]);

	LeftWindow->add_separator();
	GLUI_Panel *projectionPanel = LeftWindow->add_panel("Projections");
	GLUI_RadioGroup *projectionGrp = LeftWindow->add_radiogroup_to_panel(projectionPanel, &ProjectionType, 1, Projection);
	GLUI_RadioButton *NoProj = LeftWindow->add_radiobutton_to_group(projectionGrp, "No Projection");
	
	GLUI_RadioButton *perspectiveProj = LeftWindow->add_radiobutton_to_group(projectionGrp, "Perspective");
	LeftWindow->add_separator_to_panel(projectionPanel);
	//Not sure whats the exact value for Perspective projection Hence a the whole control on screen
	GLUI_StaticText *Text2 = LeftWindow->add_statictext_to_panel(projectionPanel, "Perspective projection Values");
	fovySp = new GLUI_Spinner(projectionPanel, "Fovy ", GLUI_SPINNER_FLOAT, 1, ProjectionPerspective);
	fovySp->set_float_limits(65,120, GLUI_LIMIT_CLAMP);
	fovySp->set_float_val(fovy);
	AspectSp = new GLUI_Spinner(projectionPanel, "Aspect", GLUI_SPINNER_FLOAT, 2, ProjectionPerspective);
	AspectSp->set_float_limits(1, 3, GLUI_LIMIT_CLAMP);
	AspectSp->set_float_val(aspect);
	NearSp = new GLUI_Spinner(projectionPanel, "zNear", GLUI_SPINNER_FLOAT, 3, ProjectionPerspective);
	NearSp->set_float_limits(0.001, 4, GLUI_LIMIT_CLAMP);
	NearSp->set_float_val(zNear);
	FarSp= new GLUI_Spinner(projectionPanel, "zFar", GLUI_SPINNER_FLOAT, 4, ProjectionPerspective);
	FarSp->set_float_limits(20, 100, GLUI_LIMIT_CLAMP);
	FarSp->set_float_val(zfar);

	
	
	GLUI_RadioButton *OrthogonalProj = LeftWindow->add_radiobutton_to_group(projectionGrp, "Orthogonal");
	GLUI_StaticText *Text = LeftWindow->add_statictext_to_panel(projectionPanel, "Orthogonal Values");
	//Not sure whats the exact value for Orthogonal projection Hence a the whole control on screen.
	// A probable bug lies here somewhere. 
	OleftSp = new GLUI_Spinner(projectionPanel, "Left", GLUI_SPINNER_FLOAT, 1, OrthoProjection);
	OleftSp->set_float_limits(-12, 12, GLUI_LIMIT_CLAMP);
	OleftSp->set_float_val(Oleft);
	OrightSp = new GLUI_Spinner(projectionPanel, "right ", GLUI_SPINNER_FLOAT, 2, OrthoProjection);
	OrightSp->set_float_limits(-12, 12, GLUI_LIMIT_CLAMP);
	OrightSp->set_float_val(Oright);
	ObottomSp = new GLUI_Spinner(projectionPanel, "Bottom", GLUI_SPINNER_FLOAT, 3, OrthoProjection);
	ObottomSp->set_float_limits(-12, 12, GLUI_LIMIT_CLAMP);
	ObottomSp->set_float_val(Obottom);
	//syntax glOrtho(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar);
	OTopSp = new GLUI_Spinner(projectionPanel, "Top", GLUI_SPINNER_FLOAT, 4, OrthoProjection);
	OTopSp->set_float_limits(-12, 12, GLUI_LIMIT_CLAMP);
	OTopSp->set_float_val(OTop);

	OzNearSp= new GLUI_Spinner(projectionPanel, "zNear", GLUI_SPINNER_FLOAT, 5, OrthoProjection);
	OzNearSp->set_float_limits(0, 7, GLUI_LIMIT_CLAMP);
	OzNearSp->set_float_val(OzNear);

	OzFarSp= new GLUI_Spinner(projectionPanel, "zFar", GLUI_SPINNER_FLOAT, 6, OrthoProjection);
	OzFarSp->set_float_limits(10, 100, GLUI_LIMIT_CLAMP);
	OzFarSp->set_float_val(OzFar);
	LeftWindow->add_separator();
	
	//Doesn't reset correctly, probably a bug getting introduced. Hence comment. 
	//gluiWindow->add_button("RESET ALL", -1, Reset);
	
	gluiWindow->add_button("QUIT", -1, Exit);
	gluiWindow->set_main_gfx_window(glutWindow);
	LeftWindow->set_main_gfx_window(glutWindow);
	GLUI_Master.auto_set_viewport();
	//Loop forever
	glutMainLoop();
	return EXIT_SUCCESS;
}

