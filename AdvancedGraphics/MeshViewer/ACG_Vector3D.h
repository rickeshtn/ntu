#ifndef ACG_VECTOR3D_H
#define ACG_VECTOR3D_H


class ACG_Vector3D
{
public:
	ACG_Vector3D(double, double, double);
	ACG_Vector3D();
	~ACG_Vector3D();
	ACG_Vector3D(const ACG_Vector3D&);
	ACG_Vector3D operator+(ACG_Vector3D&);
	ACG_Vector3D operator-(ACG_Vector3D&);
	void operator=(const ACG_Vector3D&);
	ACG_Vector3D operator*(double&);
	ACG_Vector3D operator /(double&);
	static double dotProduct(ACG_Vector3D&, ACG_Vector3D&);
	static ACG_Vector3D crossProduct(ACG_Vector3D&, ACG_Vector3D&);
	double length();
	bool setVertex(double, double, double);
	bool getVertex(double&, double&, double&);
	double x;
	double y;
	double z;
};


#endif // ACG_VECTOR3D_H
