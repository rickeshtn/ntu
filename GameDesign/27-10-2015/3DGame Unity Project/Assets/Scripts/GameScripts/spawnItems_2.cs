﻿using UnityEngine;
using System.Collections;
using Leap;

public class spawnItems_2 : MonoBehaviour
{
    public bool checkLeapIssueAfterWin = false;
    public bool graphCreated = false;
    //public LeapInterFace LeapInterface;
    public PanelManager GameRestart;
    public LeapInterFace leapball;
    //public mouseInterface MouseBall;
    public Transform[] SpawnPoints;
    public GameObject[] Coins;
    public LineRenderer[] edges, line_feedback;
    //public Light[] starLight;
    public Renderer[] starColor;
    public GameObject[] flyStarsAnim;
    //public LineRenderer feedback;
    //public GameObject[] wormhole;
    public scoreInfo ss;
    public timeInfo tt;
    //public MovieTexture explosion;
    //public GameObject GameExplosion;
    public lvlNumber levelNumb;
    public GUIText LevelNo;
    public GameObject LevelDisp;
    public GameObject TimeDisp;
    public AudioSource StarAudio;
    public mode TranquilMode;
    public int[,] edge_number;// Dear future developer, set this to false and change it in start script based upon the output from previous scene. 
    //If user had reached the game play through tranquail mode set it to true. 
    //For now I(Rickesh) am setting it to true. As I want to test the tranquail mode. 


    public float intTime = 10f;
    public int[] spawnIndex;
    public int no_stars, no_edges;
    public int[,] adjac_matrix;
    public bool graph_done = false;
    bool eggIsOn = false;
    int egg_num;
    int time1, time2, time3, win_time;
    int old_star, current_edge;
    bool first_flag = true;
    bool win_flag = false;
    Vector3 orig_sc;

    //Color orig_color;
    //public float spawnTime=2f;
    //public int no_stars=5;

    // Use this for initialization
    void Start()
    {
        if(!TranquilMode.modeselected())
        {
        //((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
            int i;
        //LevelDisp = GameObject.Find("LevelNumber");
        LevelDisp.gameObject.transform.localPosition = new Vector3(-350,-350,0);
        //InvokeRepeating ("SpawnCoin", intTime, spawnTime);
        Invoke("SpawnCoin", intTime);
        Renderer rr;
        for (i=0; i<no_stars; i++) {
            rr = Coins [i].GetComponent<Renderer> ();
            rr.material.shader = Shader.Find ("Specular");
            rr.material.SetColor ("_Color", Color.red);
        }
        Transform ttt = Coins[0].GetComponent<Transform>();
        orig_sc = ttt.localScale;
        for (int l = 0; l < 7; l++)
        {
            //flyStarsAnim[l].SetActive(true);
            flyStarsAnim[l].SetActive(false);
        }
        }
        else
        {
            int i;
            //InvokeRepeating ("SpawnCoin", intTime, spawnTime);
            int LevelValue = int.Parse(levelNumb.levelNum().ToString(), System.Globalization.NumberStyles.Integer);
            print("level Number " + LevelValue);
            Invoke("TranqSpawnCoin", intTime);
            Renderer rr;
            LevelNo.text = levelNumb.levelNum().ToString();
            TimeDisp.gameObject.transform.localPosition = new Vector3(-500, -500, 0);

            for (i = 0; i < no_stars; i++)
            {
                rr = Coins[i].GetComponent<Renderer>();
                rr.material.shader = Shader.Find("Specular");
                rr.material.SetColor("_Color", Color.red);
            }
            Transform ttt = Coins[0].GetComponent<Transform>();
            orig_sc = ttt.localScale;

        }
      
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("Menu Scene");
        }

        if (!TranquilMode.modeselected())
        {
            Vector3 orig = new Vector3(0, 0, 12);
            Vector3 haha2 = new Vector3(90, 90, 90);
            Vector3 haha = new Vector3(3, 3, 3);
            {
                if (win_flag)
                {
                    //MouseBall.capsule.transform.position = new Vector3(-400, -500, 0);
                    leapball.gameObject.transform.position = new Vector3(-400, -500, -9);

                    if (tt.currentTime <= (win_time - 2))
                    {
                        win_flag = false;

                        for (int i = 0; i < Coins.Length; i++)
                        {
                            Transform LALA = Coins[i].GetComponent<Transform>();
                            LALA.localScale = orig_sc;

                            Coins[i].SetActive(false);
                        }

                        // NEED TO ADD: clear animation as a feedback of completing a graph
                        
                        first_flag = true;
                        SpawnCoin();
                    }
                    else
                    {
                        for (int i = 0; i < no_stars; i++)
                        {
                            Transform tcoin = Coins[i].GetComponent<Transform>();
                            tcoin.Translate(orig * Time.deltaTime);
                            tcoin.Rotate(Vector3.forward, 10f * Time.deltaTime);

                            tcoin.localScale = tcoin.localScale + haha * Time.deltaTime;

                        }
                    }
                }
                else
                {
                    if (tt.currentTime % 4 == 0 && !eggIsOn && !first_flag && no_stars >= 6 && ((time3 - tt.currentTime) >= 3))
                    {
                        disp_easterEgg();
                    }
                    cancel_easterEgg();
                    check_edge();
                }
            }
        }
        else
        {
            Vector3 orig = new Vector3(0, 0, 12);
            Vector3 haha2 = new Vector3(90, 90, 90);
            Vector3 haha = new Vector3(3, 3, 3);
            TranqCheckedge();
           }
    }
    void disp_easterEgg()
    {
        //print ("!!!!!!!!!!!!!!!!!!!!!!!!!!haha" + first_flag);
        int no_left_line = 0;
        bool find_flag = false;
        for (int i = 0; i < no_stars; i++)
        {
            no_left_line = 0;

            for (int j = 0; j < no_stars; j++)
            {
                if (adjac_matrix[i, j] == 1)
                {
                    no_left_line++;
                    if (no_left_line >= 2 && i != old_star && adjac_matrix[i, old_star] == 0)
                    {
                        find_flag = true;
                        egg_num = i;
                        break;
                    }
                }
            }
            if (find_flag)
                break;
        }
        Vector3 sc = new Vector3(2, 2, 2);
        if (find_flag)
        {
            eggIsOn = true;
            time1 = tt.currentTime;

            //change the color
            /*Renderer rend = Coins [egg_num].GetComponent<Renderer> ();
            rend.material.shader = Shader.Find ("Specular");
            rend.material.SetColor ("_SpecColor", Color.blue);
            print ("change color" + egg_num);*/
            Transform eggt = Coins[egg_num].GetComponent<Transform>();
            eggt.localScale = sc;
            //starColor[egg_num].material.color = new Color(1, 0, 0);
            ParticleAnimator starFireColor = starColor[egg_num].GetComponent<ParticleAnimator>();
            Color[] selColors = starFireColor.colorAnimation;
            selColors[0] = Color.red;
            selColors[1] = Color.red;
            selColors[2] = Color.red;
            selColors[3] = Color.red;
            selColors[4] = Color.red;
            starFireColor.colorAnimation = selColors;
            //starColor[egg_num].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(1, 0, 0));
            //starLight[egg_num].color = new Color(1, 0, 0);
        }

    }
    void cancel_easterEgg()
    {
        if (eggIsOn)
        {
            time2 = tt.currentTime;
            time3 = time1 - time2;
            if (time3 >= 2)
            {
                //print ("haha"+tt.currentTime);
                eggIsOn = false;
                /*Renderer rend = Coins [egg_num].GetComponent<Renderer> ();
                rend.material.shader = Shader.Find ("Specular");
                rend.material.SetColor ("_Color",Color.red);*/
                Transform eggt = Coins[egg_num].GetComponent<Transform>();
                eggt.localScale = orig_sc;
                //starColor[egg_num].material.color = new Color(1, 1, 1);
                ParticleAnimator starFireColor = starColor[egg_num].GetComponent<ParticleAnimator>();
                Color[] selColors = starFireColor.colorAnimation;
                selColors[0] = Color.yellow;
                selColors[1] = Color.yellow;
                selColors[2] = Color.yellow;
                selColors[3] = Color.yellow;
                selColors[4] = Color.yellow;
                starFireColor.colorAnimation = selColors;
                //starColor[egg_num].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(1, 1, 0));
                //starLight[egg_num].color = new Color(1, 1, 1);
            }
        }
    }
    void check_edge()
    {

        int numMouse = clickOn_ball(); // clickOn_ball();
        int numLeap = LeapClickOn_ball();
        int num = -1;
        if ((numMouse >= 0) || (numLeap >= 0))
        {
            if (numMouse >= 0)
                num = numMouse;
            else
            {
                num = numLeap;
                Debug.Log("numLeap ");
                Debug.Log(numLeap);
            }
        }
        if (num >= 0)
        {

            if (first_flag)
            {
                current_edge = 0;
                old_star = num;
                first_flag = false;
                print("start point" + old_star);
                //starLight[num].color = new Color(0, 1, 0);
                //starColor[num].material.color = new Color(0, 1, 0);
                //starColor[num].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(0, 1, 0));
                ParticleAnimator starFireColor = starColor[num].GetComponent<ParticleAnimator>();
                Color[] selColors = starFireColor.colorAnimation;
                selColors[0] = Color.green;
                selColors[1] = Color.green;
                selColors[2] = Color.green;
                selColors[3] = Color.green;
                selColors[4] = Color.green;
                starFireColor.colorAnimation = selColors;
                StarAudio.Play();
                //edges[current_edge].SetWidth(0.5f, 0.5f);
                //edges[current_edge].SetColors(Color.white, Color.white);
            }
            else
            {
                if (adjac_matrix[num, old_star] == 1)
                {
                    if (eggIsOn && num == egg_num)
                    {
                        if (tt.currentTime < time1) time1 = tt.currentTime;
                        winGraph();

                    }
                    else
                    {
                        //Debug.Log("got one edge");
                        adjac_matrix[num, old_star] = 2;
                        adjac_matrix[old_star, num] = 2;
                        //change color
                        line_feedback[current_edge].SetPosition(0, SpawnPoints[spawnIndex[old_star]].position);
                        line_feedback[current_edge].SetPosition(1, SpawnPoints[spawnIndex[num]].position);
                        line_feedback[current_edge].SetWidth(0.6f, 0.6f);
                        //edges[current_edge].SetPosition(0, SpawnPoints[spawnIndex[old_star]].position);
                        //edges[current_edge].SetPosition(1, SpawnPoints[spawnIndex[num]].position);
                        //edges[current_edge].SetWidth(0.6f, 0.6f);
                        //edges[current_edge].SetColors(Color.green, Color.green);
                        //starLight[num].color = new Color(0, 1, 0);
                        //starColor[num].material.color = new Color(0, 1, 0);
                        //starColor[num].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(0, 1, 0));
                        ParticleAnimator starFireColor = starColor[num].GetComponent<ParticleAnimator>();
                        Color[] selColors = starFireColor.colorAnimation;
                        selColors[0] = Color.green;
                        selColors[1] = Color.green;
                        selColors[2] = Color.green;
                        selColors[3] = Color.green;
                        selColors[4] = Color.green;
                        starFireColor.colorAnimation = selColors;
                        StarAudio.Play();
                        current_edge++;
                        print("current Time " + tt.currentTime + "allowed time " + tt.allowedTime);
                        if (check_graph())
                        {
                            winGraph();
                        }
                        else if (no_way(num))
                        {
                            loseGraph();
                        }
                        else
                        {
                            old_star = num;

                        }
                        //print("old_star change to "+num);
                    }
                }
                else if (adjac_matrix[num, old_star] == 2)
                {//repeat
                    loseGraph();
                    Debug.Log("oops,repeated");
                }
            }
        }
    }

    void TranqCheckedge()
    {
        print("In Update at TranqCheckedge ");
        int numMouse = clickOn_ball();
        int numLeap = LeapClickOn_ball();
        int num = -1;
        if ((numMouse >= 0) || (numLeap >= 0))
        {
            if (numMouse >= 0)
                num = numMouse;
            else
            {
                num = numLeap;
                Debug.Log("numLeap ");
                Debug.Log(numLeap);
            }
        }
        if (num >= 0)
        {
            if (first_flag)
            {
                current_edge = 0;
                old_star = num;
                first_flag = false;
                print("start point" + old_star);
                //starLight[num].color = new Color(0, 1, 0);
                //starColor[num].material.color = new Color(0, 1, 0);
                //starColor[num].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(0, 1, 0));
                ParticleAnimator starFireColor = starColor[num].GetComponent<ParticleAnimator>();
                Color[] selColors = starFireColor.colorAnimation;
                selColors[0] = Color.green;
                selColors[1] = Color.green;
                selColors[2] = Color.green;
                selColors[3] = Color.green;
                selColors[4] = Color.green;
                starFireColor.colorAnimation = selColors;
                StarAudio.Play();
                //edges[current_edge].SetWidth(0.5f, 0.5f);
                //edges[current_edge].SetColors(Color.white, Color.white);
            }
            else
            {
                if (adjac_matrix[num, old_star] == 1)
                {
                    {
                        //Debug.Log("got one edge");
                        adjac_matrix[num, old_star] = 2;
                        adjac_matrix[old_star, num] = 2;
                        //change color
                        line_feedback[current_edge].SetPosition(0, SpawnPoints[spawnIndex[old_star]].position);
                        line_feedback[current_edge].SetPosition(1, SpawnPoints[spawnIndex[num]].position);
                        line_feedback[current_edge].SetWidth(0.6f, 0.6f);
                        //edges[current_edge].SetPosition(0, SpawnPoints[spawnIndex[old_star]].position);
                        //edges[current_edge].SetPosition(1, SpawnPoints[spawnIndex[num]].position);
                        //edges[current_edge].SetWidth(0.6f, 0.6f);
                        //edges[current_edge].SetColors(Color.green, Color.green);
                        //starLight[num].color = new Color(0, 1, 0);
                        //starColor[num].material.color = new Color(0, 1, 0);
                        //starColor[num].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(0, 1, 0));
                        ParticleAnimator starFireColor = starColor[num].GetComponent<ParticleAnimator>();
                        Color[] selColors = starFireColor.colorAnimation;
                        selColors[0] = Color.green;
                        selColors[1] = Color.green;
                        selColors[2] = Color.green;
                        selColors[3] = Color.green;
                        selColors[4] = Color.green;
                        starFireColor.colorAnimation = selColors;
                        StarAudio.Play();
                        current_edge++;

                        if (check_graph())
                        {
                            winGraph();
                        }
                        else if (no_way(num))
                        {
                            loseGraph();
                        }
                        else
                        {
                            old_star = num;

                        }
                        //print("old_star change to "+num);
                    }
                }
                else if (adjac_matrix[num, old_star] == 2)
                {//repeat
                    loseGraph();
                    Debug.Log("oops,repeated");
                }

            }
        }
    }

    int clickOn_ball()//whether mouse click on a ball
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        Ray toMouse = new Ray();
        toMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.Log("mouse position");
        Debug.Log(Input.mousePosition);
        Debug.Log("To Mouse");
        Debug.Log(toMouse);
        RaycastHit rhInfo;
        bool didHit = Physics.Raycast(toMouse, out rhInfo, 500.0f);
        if (didHit)
        {
            for (int i = 0; i < no_stars; i++)
            {
                Debug.Log("SpawnPoints");
                Debug.Log("Location "); Debug.Log(i);
                Debug.Log(SpawnPoints[spawnIndex[i]].position);
                Debug.Log("From Mouse ");
                Debug.Log(rhInfo.collider.transform.position);
                float range = 1.0f;
                bool test = ((SpawnPoints[spawnIndex[i]].position.x - range < rhInfo.collider.transform.position.x)
                             && (SpawnPoints[spawnIndex[i]].position.x + range > rhInfo.collider.transform.position.x))
                                && ((SpawnPoints[spawnIndex[i]].position.y - range < rhInfo.collider.transform.position.y)
                                    && (SpawnPoints[spawnIndex[i]].position.y + range > rhInfo.collider.transform.position.y));

                if (test)
                {//!!use spawnIndex instead of coins[i]
                    print("CLICK ON BALL" + i);
                    return i;
                }
            }
            return -1;
        }
        //	return -1;
        //}
        return -1;
    }

    /*int newclickOn_ball()
    {
        if (MouseBall.click_ball >= 0)
        {
            int Mouse_Click = MouseBall.click_ball;
            print("SPAWN:click on ball" + MouseBall.click_ball);
            MouseBall.click_ball = -1;
            return Mouse_Click;
        }
        else
            return -1;
    }*/

    int LeapClickOn_ball()
    {
        if (leapball.click_ball >= 0)
        {
            int leapBall_Click = leapball.click_ball;
            //print("SPAWN:click on ball" + leapball.click_ball);
            //StarAudio.Play();
            leapball.click_ball = -1;
            return leapBall_Click;
        }
        else
            return -1;
    }


    bool check_graph()//whether complete a graph
    {
        for (int i = 0; i < no_stars; i++)
        {
            for (int j = 0; j < no_stars; j++)
            {
                if (adjac_matrix[i, j] == 1)
                {// has not been visited
                    return false;
                }
            }
        }
        if (eggIsOn && !TranquilMode.modeselected())
        {


            eggIsOn = false;
            /*Renderer rend = Coins [egg_num].GetComponent<Renderer> ();
                rend.material.shader = Shader.Find ("Specular");
                rend.material.SetColor ("_Color",Color.red);*/
            Transform eggt = Coins[egg_num].GetComponent<Transform>();
            eggt.localScale = orig_sc;
            ParticleAnimator starFireColor = starColor[egg_num].GetComponent<ParticleAnimator>();
            Color[] selColors = starFireColor.colorAnimation;
            selColors[0] = Color.yellow;
            selColors[1] = Color.yellow;
            selColors[2] = Color.yellow;
            selColors[3] = Color.yellow;
            selColors[4] = Color.yellow;
            starFireColor.colorAnimation = selColors;
            //starColor[egg_num].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(1, 1, 0));
            //starColor[egg_num].material.color = new Color(1, 1, 1);
            //starLight[egg_num].color = new Color(1, 1, 1);
        }
        graph_done = true;
        return true;
    }

    bool no_way(int a)
    {
        //bool flag = true;
        for (int k = 0; k < no_stars; k++)
        {
            if (adjac_matrix[k, a] == 1)
            {
                return false;
            }
        }
        Debug.Log("oops,no way ");
        return true;
    }

    public void loseGraph()
    {
        Debug.Log("KO ");
        disappear_graph();
        //GameExplosion.transform.position = new Vector3 (0, 0, 3);
        //GameExplosion.transform.Translate(0.0f, 0.0f, 3.0f);
        //explosion.Play ();
        //		if (explosion.isPlaying ()) {
        //
        //		}
        //if (explosion.isPlaying == true && explosion.duration == 3) {
        //GameExplosion.transform.Translate (13.0f, 0.0f, 3.0f);
        //GameExplosion.transform.position = new Vector3 (13, 0, 3);
        GameRestart.transform.position = new Vector3(0, 0, 3);
        TimeDisp.gameObject.transform.localPosition = new Vector3(-500, -500, 0);
        //		if(GameRestart.)

        //}
        // NEED TO ADD: time stop
        // NEED TO ADD: clear animation as a feedback of completing a graph
        //NEED TO ADD:show "game over screen"(including scores,play again button, quit button)

    }

    public void ReloadAfterLoose()
    {
        //update score
        ss.UpdateScoreText(0 * no_edges);
        disappear_graph();
        first_flag = true;
        SpawnCoin();
        for (int i = 0; i < no_stars; i++)
        {
            flyStarsAnim[i].SetActive(false);
        }
    }
    void winGraph()
    {
        Debug.Log("finish graph,yah");

        if (TranquilMode.modeselected())
        {
            Debug.Log("KO ");
            disappear_graph();
            ss.UpdateScoreText(1000 * no_edges);
            GameRestart.transform.position = new Vector3(0, 0, 3);
            TimeDisp.gameObject.transform.localPosition = new Vector3(-500, -500, 0);
            leapball.click_ball = -1;
            //MouseBall.click_ball = -1;

            for (int i = 0; i < 7; i++)
            {
                //flyStarsAnim[i].SetActive(!(flyStarsAnim[i].active));
                flyStarsAnim[i].SetActive(true);
            }
        }
        else
        {
            //update score
            checkLeapIssueAfterWin = true;
            ss.UpdateScoreText(1000 * no_edges);
            leapball.click_ball = -1;
            //MouseBall.click_ball = -1;
            //MouseBall.capsule.transform.position = new Vector3(-400, -500, 0);
            win_time = tt.currentTime;
            win_flag = true;
            for (int i = 0; i < edges.Length; i++)
            {
                edges[i].enabled = false;
            }
            for (int i = 0; i < line_feedback.Length; i++)
                line_feedback[i].SetWidth(0f, 0f);

            for (int i = 0; i < 7; i++)
            {
                //flyStarsAnim[i].SetActive(!(flyStarsAnim[i].active));
                flyStarsAnim[i].SetActive(true);
            }
        }
    }
    void disappear_graph()
    {
        if (eggIsOn && !TranquilMode.modeselected())
        {


            eggIsOn = false;
            /*Renderer rend = Coins [egg_num].GetComponent<Renderer> ();
                rend.material.shader = Shader.Find ("Specular");
                rend.material.SetColor ("_Color",Color.red);*/
            Transform eggt = Coins[egg_num].GetComponent<Transform>();
            eggt.localScale = orig_sc;
            ParticleAnimator starFireColor = starColor[egg_num].GetComponent<ParticleAnimator>();
            Color[] selColors = starFireColor.colorAnimation;
            selColors[0] = Color.yellow;
            selColors[1] = Color.yellow;
            selColors[2] = Color.yellow;
            selColors[3] = Color.yellow;
            selColors[4] = Color.yellow;
            starFireColor.colorAnimation = selColors;
            //starColor[egg_num].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(1, 1, 0));
            //starColor[egg_num].material.color = new Color(1, 1, 1);
            //starLight[egg_num].color = new Color(1, 1, 1);
        }

        for (int i = 0; i < Coins.Length; i++)
        {
            Transform LALA = Coins[i].GetComponent<Transform>();
            LALA.localScale = orig_sc;

            Coins[i].SetActive(false);

        }
        for (int i = 0; i < edges.Length; i++)
        {
            edges[i].enabled = false;
        }
        for (int i = 0; i < line_feedback.Length; i++)
            line_feedback[i].SetWidth(0f, 0f);
    }


    void TranqSpawnCoin()
    {
        print("In Update at TranquilMode TranqSpawnCoin");
        //int[] spawnIndex;// set the index of arr randomly
       int  LevelValue = int.Parse(LevelNo.text.ToString(), System.Globalization.NumberStyles.Integer);
       int RangeLevelMax = LevelValue + 2;
       int RangeLevelMin = LevelValue - 2;
        int i, j, k, t;
        int max;
        bool flag = false;
        graph_done = false;

        //if ((tt.allowedTime * 0.7 < tt.currentTime) && (TranquilMode == false))
        //{
        //    no_stars = Random.Range(4, 7);//number of stars}
        //}
        //else
        //{
        no_stars = Random.Range(5,8);
        //}
        //no_stars = 3;
            if (LevelValue == 1)
            {
                no_stars = 3;
                no_edges = 3;
            }
            else if (LevelValue <= 4)
            {
                no_stars = 4;
                no_edges = LevelValue+1;
                print(" NO of stars " + no_stars + "No of edges " + no_edges + "LevelValue" + LevelValue);
                //Debug.Break();
            }
            else if (LevelValue <= 7)
            {
                no_stars = 5;
                no_edges = LevelValue;

            }
            else if (LevelValue <= 10)
            {
                no_stars = 5;
                no_edges = LevelValue;

            }

            else if (LevelValue <= 20)
            {
                no_stars = 6;
                no_edges = LevelValue - 5;
            }
            else if (LevelValue <= 35)
            {
                no_stars = 7;
                no_edges = LevelValue - 20;
            }
        /*    else 
         if(LevelValue<=56)
        {
         no_stars=8;
         no_edges= LevelValue-28;
        }*/

        no_edges = number_spawn(no_stars, no_edges);
    
       
      

    }
   int number_spawn(int no_stars, int no_edges)
    {
        
        int i, j, k, t;
        int max;
        bool flag = false;
        graph_done = false;
        spawnIndex = new int[no_stars];

        adjac_matrix = new int[no_stars, no_stars];
        edge_number = new int[no_stars, no_stars];
        int[] degree = new int[no_stars];
        int node1, node2, no_odd, e, f;
        int[] comp = new int[no_stars];
        for (i = 0; i < no_stars; i++)
        {
            degree[i] = 0;
            comp[i] = 0;
            for (j = 0; j < no_stars; j++)
            {
                adjac_matrix[i, j] = 0;
                edge_number[i,j]=0;
            }
        }
        k = 0;


        //create adjacent matrix
    if (no_stars == 4|| (no_stars==5&& no_edges>6)||((no_stars==7&& no_edges>16))) {
            int preNode=-1;
            for(i=0;i<no_stars;i++)
            {
                for (j=0;j<no_stars;j++)
                {
                    if(i==j)
                    {
                        adjac_matrix[i,j]=0;
                    }
                    else{
                        adjac_matrix[i,j]=1;
                        degree[i]++;
                        degree[j]++;
                    }

                }
            }
            if(no_stars==4)
            {
                for(t=0;t<(6-no_edges);t++)
               {
                //delete an edge randomly
                i=Random.Range(0,no_stars-1);
                j=Random.Range(0,no_stars-1);
                while(adjac_matrix[i,j]==0)
                {j=Random.Range(0,no_stars-1);}
                adjac_matrix[i,j]=0;
                adjac_matrix[j,i]=0;
                    degree[i]--;
                    degree[j]--;
               }
            }
            else{
                int totalEdge=no_stars*(no_stars-1)/2;

                for(t=0;t<(totalEdge-no_edges);t++)
                {

                    //delete an edge randomly
                    if(t==0)
                    {
                        i=Random.Range(0,no_stars-1);

                    }
                    else i=preNode;
                    j=Random.Range(0,no_stars-1);
                    while(adjac_matrix[i,j]==0||degree[j]<=1)
                    {j=Random.Range(0,no_stars-1);}
                    adjac_matrix[i,j]=0;
                    adjac_matrix[j,i]=0;
                    degree[i]--;
                    degree[j]--;
                    int iRandom=Random.Range(0,1);
                    if(iRandom==1)
                        preNode=i;
                    else
                        preNode=j;

                }

              }
        }
    


        /*if (no_stars>=5&& no_edges< (no_stars+no_stars-3)) {
            int oddNode2=-1;
            int oddNode=-1;
            int evenNode=-1;

            for(i=0;i<no_stars;i++)
            {
                if(i==(i+1)%no_stars)
                {
                    print ("diagonal in new4");
                }

                adjac_matrix[i,(i+1)%no_stars]=1;
                adjac_matrix[(i+1)%no_stars,i]=1;
                degree[i]++;
                degree[(i+1)%no_stars]++;
                    
                    

            }

                
            if(no_edges>=(no_stars+1))
            {
                //choose first two node
                oddNode=Random.Range(0,no_stars-1);
                oddNode2=Random.Range(0,no_stars-1);
               while(adjac_matrix[oddNode,oddNode2]==1 || oddNode==oddNode2)
               {
                oddNode2=Random.Range(0,no_stars-1);
               }
                if(oddNode==oddNode2)
                {
                    print ("diagonal in new3");
                }
                adjac_matrix[oddNode,oddNode2]=1;
                adjac_matrix[oddNode2,oddNode]=1;
                degree[oddNode]++;
                degree[oddNode2]++;


                int minDegree;
                
                for(t=1;t<(no_edges- no_stars-1);t++)
                {
                    minDegree=100;

                    //evenNode=Random.Range(0,no_stars-1);
                    for(i=0;i<no_stars;i++)
                    {
                        if(adjac_matrix[oddNode,i]==0 && degree[i]%2==0&& i!=oddNode)
                        {
                            if(degree[i]<=minDegree)
                            {
                                minDegree=degree[i];
                                evenNode=i;
                            }
                        }
                        

                    }

                    if(degree[evenNode]>=no_stars-2||minDegree==100)
                    {
                        print ("Not find the evenNode");
                        break;
                    }

                    else{
                        if(oddNode==evenNode)
                        {
                            print ("diagonal in new2");
                        }
                        adjac_matrix[oddNode,evenNode]=1;
                        adjac_matrix[evenNode,oddNode]=1;
                        degree[oddNode]++;
                        degree[evenNode]++;
                        
                        oddNode=evenNode;

                    
                    }
                    if(degree[oddNode]==no_stars-1)
                        
                    {
                    print("wrong");
                    }
                    
                }

               if(no_edges>=no_stars+2)
                {
                    if(adjac_matrix[oddNode2,oddNode]==1)// link oddNode2 and another evenNode
                    {
                        minDegree=100;
                        oddNode=oddNode2;
                        for(i=0;i<no_stars;i++)
                        {
                            if(adjac_matrix[oddNode,i]==0 && degree[i]%2==0&& i!= oddNode)
                            {
                                if(degree[i]<=minDegree)
                                {
                                    minDegree=degree[i];
                                    evenNode=i;
                                }
                            }
                            
                            
                        }
                        if(degree[evenNode]>=no_stars-2)
                        {
                            print ("Not find the evenNode in 2");
                        }
                        else{
                            if(oddNode==evenNode)
                            {
                                print ("diagonal in new1");
                            }
                        
                        adjac_matrix[oddNode,evenNode]=1;
                        adjac_matrix[evenNode,oddNode]=1;
                        degree[oddNode]++;
                        degree[evenNode]++;
                        }
                        
                    }
                    else
                    {

                        //print ("odd"+oddNode2);
                        //print ("even"+evenNode);
                        adjac_matrix[oddNode2,oddNode]=1;
                        adjac_matrix[oddNode,oddNode2]=1;
                        degree[oddNode2]++;
                        degree[oddNode]++;
                        print ("zero");
                    }

                
                }

               }

                

        }*/
        else{
            creatMatrix (no_stars, no_edges);}
        //display stars
        for (i = 0; i < no_stars; i++)
        {
            spawnIndex[i] = Random.Range(0, SpawnPoints.Length - 1);
            while (true)
            {
                for (j = 0; j < i; j++)
                {
                    if (spawnIndex[j] == spawnIndex[i])
                    {
                        flag = true;
                        break;
                    }
                }

                if (flag)
                {
                    spawnIndex[i] = Random.Range(0, SpawnPoints.Length - 1);//!!!!bug1
                    flag = false;
                }
                else
                    break;
            }

            Coins[i].SetActive(true);
            //Instantiate (Coins [i], SpawnPoints [spawnIndex [i]].position, SpawnPoints [spawnIndex [i]].rotation);
            Coins[i].transform.position = SpawnPoints[spawnIndex[i]].position;
            ParticleAnimator starFireColor = starColor[i].GetComponent<ParticleAnimator>();
            Color[] selColors = starFireColor.colorAnimation;
            selColors[0] = Color.yellow;
            selColors[1] = Color.yellow;
            selColors[2] = Color.yellow;
            selColors[3] = Color.yellow;
            selColors[4] = Color.yellow;
            starFireColor.colorAnimation = selColors;
            //starColor[i].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(1, 1, 0));
            //starColor[i].material.color = new Color(1, 1, 1);
            //starLight[i].color = new Color(1, 1, 1);
            //Transform tc= Coins[i].GetComponent<Transform>();
            //tc.position=SpawnPoints[spawnIndex[i]].position;

        }

        //display  edges


        k = 0;
        int sum = 0;
        for (i = 0; i < no_stars - 1; i++)
        {
            for (j = i + 1; j < no_stars; j++)
            {

                if (adjac_matrix[i, j] > 1)
                {
                    sum++;
                    //					Debug.Log ("repeat");Debug.Log(sum);
                }
                if (adjac_matrix[i, j] >= 1)
                {
                    adjac_matrix[i, j] = 1;
                    edges[k].enabled = true;
                    edges[k].SetPosition(0, SpawnPoints[spawnIndex[i]].position);
                    edges[k].SetPosition(1, SpawnPoints[spawnIndex[j]].position);
                    edges[k].SetWidth(0.2f, 0.2f);
                    //edges[k].SetColors(Color.white, Color.white);
                    //edges [k].material.SetColor("_SpecColor",Color.red);

                    k++;
                }
            }
        }
        //		Debug.Log ("k2");
        //		Debug.Log (k);

        for (i = 0; i < no_stars; i++)
        {
            if (adjac_matrix[i, i] != 0)
                Debug.Log("diagnal");


        }
        first_flag = true;
        // feedback.enabled = true;
        print("no_stars" + no_stars);
        print("no_edges" + no_edges);

        if (TranquilMode == false) //for now it is false in Quantum mode it was not there. 
            time3 = tt.allowedTime; //The actual syntax here is if(TranquilMode == false)
        //disappear_edge 
        /*for (i=0; i<edges.Length; i++) {
            edges [i].enabled = false;
        }*/
        return k;


      
    }
    void creatMatrix(int no_stars,int no_edges)
    {

       
        //int[] spawnIndex;// set the index of arr randomly
        int i, j, k, t;
        int max;
        bool flag = false;
        graph_done = false;
        spawnIndex = new int[no_stars];


        //create adjacent matrix
        adjac_matrix = new int[no_stars, no_stars];
        edge_number = new int[no_stars, no_stars];
        int[] degree = new int[no_stars];
        int node1, node2, no_odd, e, f;
        int[] comp = new int[no_stars];
        for (i = 0; i < no_stars; i++)
        {
            degree[i] = 0;
            comp[i] = 0;
            for (j = 0; j < no_stars; j++)
            {
                adjac_matrix[i, j] = 0;
                edge_number[i,j]=0;
            }
        }
        k = 0;
        //create a connected graph
        node1 = Random.Range(0, no_stars - 1);
        node2 = Random.Range(0, no_stars - 1);
        while (node2 == node1)
        {
            node2 = Random.Range(0, no_stars - 1);
        }
        degree[node1] += 1;
        degree[node2] += 1;
        adjac_matrix[node1, node2] += 1;
        adjac_matrix[node2, node1] += 1;
        comp[node1] = 1;
        comp[node2] = 1;
        k++;
        for (i = 0; i < no_stars - 2; i++)
        {
            for (j = 0; j < no_stars; j++)
            {
                if (comp[j] == 1 && degree[j] < no_stars / 2)
                {
                    node1 = j;
                    break;
                }
            }
            for (j = 0; j < no_stars; j++)
            {
                if (comp[j] == 0 && degree[j] < no_stars / 2)
                {
                    node2 = j;
                    break;
                }
            }
            degree[node1] += 1;
            degree[node2] += 1;
            adjac_matrix[node1, node2] += 1;
            adjac_matrix[node2, node1] += 1;
            comp[node1] = 1;
            comp[node2] = 1;
            k++;
        }

        // add edges into the connected graph
        no_odd = 0;
        for (i = 0; i < no_stars; i++)
        {
            if (degree[i] % 2 != 0)
            {
                no_odd++;
            }
        }
        
               
        
          // step1
        while (no_odd > 2)
        {
            
            
            node1 = Random.Range(0, no_stars - 1);
            while (degree[node1] % 2 == 0)
            {
                node1 = Random.Range(0, no_stars - 1);
            }
            
            t = 0;
            
            for (i = 0; i < no_stars; i++)
            {
                if (degree[i] % 2 != 0 && node1 != i && adjac_matrix[node1, i] == 0)
                {
                    node2 = i;
                    break;
                }
                
            }
            if (i == no_stars)
            {
                for (e = 0; e < no_stars; e++)
                {
                    if (degree[e] / 2 == 0)
                    {
                        for (f = 0; f < no_stars; f++)
                        {
                            if (degree[f] / 2 != 0 && adjac_matrix[f, e] == 0)
                            {
                                node1 = f;
                                node2 = e;
                                //if(adjac_matrix[node1,node2]>0)//Debug.Log ("repeat in step2 of step1");
                                //if(node1==node2)//Debug.Log ("loop in  step2 of step 1");
                                
                                degree[node1] += 1;
                                degree[node2] += 1;
                                adjac_matrix[node1, node2] += 1;
                                adjac_matrix[node2, node1] += 1;
                                k++;
                                break;
                                
                            }
                            
                        }
                        if (f < no_stars)
                            break;
                    }
                    
                }
                //if(e==no_stars) //Debug.Log ("no action in step 2 of step 1")    ;    
                
            }
            else
            {
                //if(adjac_matrix[node1,node2]>0)//Debug.Log ("repeat in step1");
                //if(node1==node2)//Debug.Log ("loop in final step1");
                degree[node1] += 1;
                degree[node2] += 1;
                adjac_matrix[node1, node2] = 1;
                adjac_matrix[node2, node1] = 1;
                k++;
                no_odd -= 2;
                if (no_edges - 1 < k)
                    no_edges = k + 1;
                
            }
        }
        //        //Debug.Log ("no_edge");
        //        //Debug.Log (no_edges);
        
        //step2
        while ((no_edges - 1) > k)
        {
            
            //choose one odd one even
            for (i = 0; i < no_stars; i++)
            {
                if (degree[i] / 2 == 0)
                {
                    for (j = 0; j < no_stars; j++)
                    {
                        if (degree[j] / 2 != 0 && adjac_matrix[j, i] == 0)
                        {
                            node1 = i;
                            node2 = j;
                            /*if (adjac_matrix [node1, node2] > 0)
                                //Debug.Log ("repeat in step2");
                            if (node1 == node2)
                                //Debug.Log ("loop in  step2");*/
                            
                            degree[node1] += 1;
                            degree[node2] += 1;
                            adjac_matrix[node1, node2] = 1;
                            adjac_matrix[node2, node1] = 1;
                            k++;
                            break;
                            
                        }
                        
                    }
                    if (j < no_stars)
                        break;
                }
                
            }
            if (i == no_stars)
            {
                no_edges = k + 1;
                ////Debug.Log ("no action in step 2"); //Debug.Log ("no_edges");//Debug.Log (no_edges);
            }
        }
        //step3:
        t = 0;
        for (i = 0; i < no_stars; i++)
        {
            if (degree[i] % 2 != 0 && t > 0)
            {
                node2 = i;
                break;
            }
            if (degree[i] % 2 != 0 && t == 0)
            {
                node1 = i;
                t++;
            }
        }
        if (adjac_matrix[node1, node2] > 0)
            
            //Debug.log
            
            Debug.Log("repeat in final step");//%%%%%%%%% this is the cause of odd degree
        else
            k++;
        //if(node1==node2)//Debug.Log ("loop in final step");
        no_odd -= 2;
        degree[node1] += 1;
        degree[node2] += 1;
        adjac_matrix[node1, node2] = 1;
        adjac_matrix[node2, node1] = 1;
        no_edges = k;
    
    
    }
    void SpawnCoin()
    {
        graphCreated = false;
        checkLeapIssueAfterWin = false;
        //int[] spawnIndex;// set the index of arr randomly
        int i, j, k, t;
        int max;
        bool flag = false;
        graph_done = false;

        if ((tt.allowedTime * 0.7 < tt.currentTime))
        {
            no_stars = Random.Range(3, 6);//number of stars}
        }
        else
        {
            no_stars = Random.Range(5, 8);
        }
        //no_stars = 3;

        max = (no_stars - 1) / 2 * no_stars;
        if (max > 16)
            max = 16;
        no_edges = Random.Range(no_stars + 1, max);
        //no_edges = 3;
        /*Debug.Log ("no_stars");
        Debug.Log (no_stars);
        Debug.Log ("no_edge");
        Debug.Log (no_edges);*/

        spawnIndex = new int[no_stars];
        //create adjacent matrix
        adjac_matrix = new int[no_stars, no_stars];
        int[] degree = new int[no_stars];
        int node1, node2, no_odd, e, f;
        int[] comp = new int[no_stars];
        for (i = 0; i < no_stars; i++)
        {
            degree[i] = 0;
            comp[i] = 0;
            for (j = 0; j < no_stars; j++)
            {
                adjac_matrix[i, j] = 0;
            }
        }
        k = 0;
        node1 = Random.Range(0, no_stars - 1);
        node2 = Random.Range(0, no_stars - 1);
        while (node2 == node1)
        {
            node2 = Random.Range(0, no_stars - 1);
        }
        degree[node1] += 1;
        degree[node2] += 1;
        adjac_matrix[node1, node2] += 1;
        adjac_matrix[node2, node1] += 1;
        comp[node1] = 1;
        comp[node2] = 1;
        k++;
        //create a connected graph
        for (i = 0; i < no_stars - 2; i++)
        {
            for (j = 0; j < no_stars; j++)
            {
                if (comp[j] == 1 && degree[j] < no_stars / 2)
                {
                    node1 = j;
                    break;
                }
            }
            for (j = 0; j < no_stars; j++)
            {
                if (comp[j] == 0 && degree[j] < no_stars / 2)
                {
                    node2 = j;
                    break;
                }
            }
            degree[node1] += 1;
            degree[node2] += 1;
            adjac_matrix[node1, node2] += 1;
            adjac_matrix[node2, node1] += 1;
            comp[node1] = 1;
            comp[node2] = 1;
            k++;
        }
        // add edges into the connected graph
        no_odd = 0;
        for (i = 0; i < no_stars; i++)
        {
            if (degree[i] % 2 != 0)
            {
                no_odd++;
            }
        }

        //		Debug.Log ("no_odd");
        //		Debug.Log (no_odd);
        //		

        // step1
        while (no_odd > 2)
        {


            node1 = Random.Range(0, no_stars - 1);
            while (degree[node1] % 2 == 0)
            {
                node1 = Random.Range(0, no_stars - 1);
            }

            t = 0;

            for (i = 0; i < no_stars; i++)
            {
                if (degree[i] % 2 != 0 && node1 != i && adjac_matrix[node1, i] == 0)
                {
                    node2 = i;
                    break;
                }

            }
            if (i == no_stars)
            {
                for (e = 0; e < no_stars; e++)
                {
                    if (degree[e] / 2 == 0)
                    {
                        for (f = 0; f < no_stars; f++)
                        {
                            if (degree[f] / 2 != 0 && adjac_matrix[f, e] == 0)
                            {
                                node1 = f;
                                node2 = e;
                                //if(adjac_matrix[node1,node2]>0)Debug.Log ("repeat in step2 of step1");
                                //if(node1==node2)Debug.Log ("loop in  step2 of step 1");

                                degree[node1] += 1;
                                degree[node2] += 1;
                                adjac_matrix[node1, node2] += 1;
                                adjac_matrix[node2, node1] += 1;
                                k++;
                                break;

                            }

                        }
                        if (f < no_stars)
                            break;
                    }

                }
                //if(e==no_stars) Debug.Log ("no action in step 2 of step 1")	;	

            }
            else
            {
                //if(adjac_matrix[node1,node2]>0)Debug.Log ("repeat in step1");
                //if(node1==node2)Debug.Log ("loop in final step1");
                degree[node1] += 1;
                degree[node2] += 1;
                adjac_matrix[node1, node2] = 1;
                adjac_matrix[node2, node1] = 1;
                k++;
                no_odd -= 2;
                if (no_edges - 1 < k)
                    no_edges = k + 1;

            }
        }
        //		Debug.Log ("no_edge");
        //		Debug.Log (no_edges);

        //step2
        while ((no_edges - 1) > k)
        {

            //choose one odd one even
            for (i = 0; i < no_stars; i++)
            {
                if (degree[i] / 2 == 0)
                {
                    for (j = 0; j < no_stars; j++)
                    {
                        if (degree[j] / 2 != 0 && adjac_matrix[j, i] == 0)
                        {
                            node1 = i;
                            node2 = j;
                            /*if (adjac_matrix [node1, node2] > 0)
                                Debug.Log ("repeat in step2");
                            if (node1 == node2)
                                Debug.Log ("loop in  step2");*/

                            degree[node1] += 1;
                            degree[node2] += 1;
                            adjac_matrix[node1, node2] = 1;
                            adjac_matrix[node2, node1] = 1;
                            k++;
                            break;

                        }

                    }
                    if (j < no_stars)
                        break;
                }

            }
            if (i == no_stars)
            {
                no_edges = k + 1;
                //Debug.Log ("no action in step 2"); Debug.Log ("no_edges");Debug.Log (no_edges);
            }
        }



        //step3:
        t = 0;
        for (i = 0; i < no_stars; i++)
        {
            if (degree[i] % 2 != 0 && t > 0)
            {
                node2 = i;
                break;
            }
            if (degree[i] % 2 != 0 && t == 0)
            {
                node1 = i;
                t++;
            }
        }
        if (adjac_matrix[node1, node2] > 0)
            Debug.Log("repeat in final step");
        else
            k++;
        //if(node1==node2)Debug.Log ("loop in final step");
        no_odd -= 2;
        degree[node1] += 1;
        degree[node2] += 1;
        adjac_matrix[node1, node2] = 1;
        adjac_matrix[node2, node1] = 1;
        no_edges = k;
        //display stars
        for (i = 0; i < no_stars; i++)
        {
            spawnIndex[i] = Random.Range(0, SpawnPoints.Length - 1);
            while (true)
            {
                for (j = 0; j < i; j++)
                {
                    if (spawnIndex[j] == spawnIndex[i])
                    {
                        flag = true;
                        break;
                    }
                }

                if (flag)
                {
                    spawnIndex[i] = Random.Range(0, SpawnPoints.Length - 1);//!!!!bug1
                    flag = false;
                }
                else
                    break;
            }

            Coins[i].SetActive(true);
            //Instantiate (Coins [i], SpawnPoints [spawnIndex [i]].position, SpawnPoints [spawnIndex [i]].rotation);
            Coins[i].transform.position = SpawnPoints[spawnIndex[i]].position;
            ParticleAnimator starFireColor = starColor[i].GetComponent<ParticleAnimator>();
            Color[] selColors = starFireColor.colorAnimation;
            selColors[0] = Color.yellow;
            selColors[1] = Color.yellow;
            selColors[2] = Color.yellow;
            selColors[3] = Color.yellow;
            selColors[4] = Color.yellow;
            starFireColor.colorAnimation = selColors;
            //starColor[i].GetComponent<ParticleRenderer>().material.SetColor("_Color", new Color(1, 1, 0));
            //starColor[i].material.color = new Color(1, 1, 1);
            //starLight[i].color = new Color(1, 1, 1);
            //Transform tc= Coins[i].GetComponent<Transform>();
            //tc.position=SpawnPoints[spawnIndex[i]].position;

        }

        //display  edges


        k = 0;
        int sum = 0;
        for (i = 0; i < no_stars - 1; i++)
        {
            for (j = i + 1; j < no_stars; j++)
            {

                if (adjac_matrix[i, j] > 1)
                {
                    sum++;
                    //					Debug.Log ("repeat");Debug.Log(sum);
                }
                if (adjac_matrix[i, j] >= 1)
                {
                    adjac_matrix[i, j] = 1;
                    edges[k].enabled = true;
                    edges[k].SetPosition(0, SpawnPoints[spawnIndex[i]].position);
                    edges[k].SetPosition(1, SpawnPoints[spawnIndex[j]].position);
                    edges[k].SetWidth(0.2f, 0.2f);
                    //edges[k].SetColors(Color.white, Color.white);
                    //edges [k].material.SetColor("_SpecColor",Color.red);

                    k++;
                }
            }
        }
        //		Debug.Log ("k2");
        //		Debug.Log (k);

        for (i = 0; i < no_stars; i++)
        {
            if (adjac_matrix[i, i] != 0)
                Debug.Log("diagnal");


        }
        first_flag = true;
        // feedback.enabled = true;
        print("no_stars" + no_stars);
        print("no_edges" + no_edges);

        time3 = tt.allowedTime;
        //disappear_edge 
        /*for (i=0; i<edges.Length; i++) {
            edges [i].enabled = false;
        }*/
        graphCreated = true;
        checkLeapIssueAfterWin = true;
        for (int l = 0; l < 7; l++)
        {
            //flyStarsAnim[l].SetActive(!(flyStarsAnim[l].active));
            flyStarsAnim[l].SetActive(false);
        }
    }

}
