﻿using UnityEngine;
using System.Collections;

public class timeInfo : MonoBehaviour {
	private GUIText textfield;
	public  int allowedTime = 80;
	public int currentTime ;
    public spawnItems_2 spawnObj;
    public gameplay QuantumPlayScene;
	// Use this for initialization
	void Start () {
		currentTime = allowedTime;
		textfield = GameObject.Find("TimeInfo").GetComponent<GUIText>();
		UpdateTimerText();
		// start the timer ticking
		StartCoroutine(TimerTick());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	IEnumerator TimerTick()
	{
		// while there are seconds left
		while (currentTime > 0) {
			// wait for 1 second
			yield return new WaitForSeconds(1);
			// reduce the time
			currentTime--;
			//Debug.Log("counting...");
			UpdateTimerText ();
		}

        if (currentTime == 0)
        {
            if (Application.loadedLevelName == "HyperSpace Play Scene")
            {
                QuantumPlayScene.loseGraph();
            }
        }
        else
            spawnObj.loseGraph();
	}
	
	void UpdateTimerText()
	{
		// update the textfield
		textfield.text = currentTime.ToString();
	}
}
