﻿using UnityEngine;
using System.Collections;

public class GameReplayButton : MonoBehaviour {
	public spawnItems_2 Spawn;
    public gameplay QuantumScene;
    public spawnItemNew1 spawn1;
    public spawnItem_new spawn2;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void gameReplay()
	{
        if (Application.loadedLevelName == "HyperSpace Play Scene")
        {
            QuantumScene.GameRestart.transform.position = new Vector3(0, -530, 3);
            Application.LoadLevel(Application.loadedLevel);
        }
        else
        {
            Spawn.GameRestart.transform.position = new Vector3(0, -530, 3);
            Application.LoadLevel(Application.loadedLevel);
        }
        
	}
}
