﻿using UnityEngine;
using System.Collections;

public class scoreInfo : MonoBehaviour {
    
    public GUIText textfield, panelScore;
    public int score=0;
   //spawnItems_2 s;
    
    // Use this for initialization
	void Start () {
        textfield = GameObject.Find("ScoreInfo").GetComponent<GUIText>();
        panelScore = GameObject.Find("ScoreDisplay").GetComponent<GUIText>();
        score = 0;
        UpdateScoreText(score);
	}
	
	// Update is called once per frame
	void Update () {
      
	}

    public void UpdateScoreText(int b)
    {
        score = score + b;
        // update textfield with score
		textfield.text = score.ToString ();
        panelScore.text = score.ToString();
    }
}
