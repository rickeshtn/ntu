﻿using UnityEngine;
using System.Collections;

public class spawnmove : MonoBehaviour {
    public spawnItemNew1 sp;
	public gameplay play;
	public gameplay2 play2;
	public mode TranquilMode;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (!TranquilMode.modeselected ()  &&!play.lose_flag &&!play2.lose_flag ) {

			if(sp.Coins[0].transform.position.z<=-1)
			{
				play.disappear_graph();
				sp.SpawnCoin();
			}
			else{
			change_stars();
			change_edges ();
			}
		}

		//t.Translate(orig * Time.deltaTime);
		//t.Rotate(Vector3.forward, 10f * Time.deltaTime);
	}
	void change_stars()
	{
		Vector3 orig;
		//Vector3 speed1 = new Vector3 (1, 0, -7);
		Vector3 speed2 = new Vector3 (-2, 0, -7);
		//Vector3 speed3 = new Vector3 (1, -1, -7);
		//Vector3 speed4 = new Vector3 (0, -1, -7);
		Vector3 speed5 = new Vector3 (-1, 0, -2);
		Vector3[] speed={speed2,speed5};
		int n = Random.Range (0, speed.Length-1);

		orig =speed[n];


		orig.z = -(3-sp.no_edges/2);
		for (int i = 0; i < sp.no_stars ; i++) {
			Transform t = sp.Coins [i].GetComponent<Transform> ();
            Vector3 TransPosition = orig * Time.deltaTime * 0.03f;
			t.Translate (TransPosition);
            //Write a if loop for making the graph go disappear at some position.. 
		}


	}
	void change_edges()
	{
		int k = 0;
		int sum = 0;
		int i;
		for (i = 0; i < sp.no_stars - 1; i++)
		{
			for (int j = i + 1; j < sp.no_stars; j++)
			{
				

				if (sp.adjac_matrix[i, j] >= 1)
				{
					//sp.adjac_matrix[i, j] = 1;
					//sp.edges[k].enabled = false;
					Transform t1 = sp.Coins [i].GetComponent<Transform> ();
					
					Transform t2 = sp.Coins [j].GetComponent<Transform> ();
					sp.edges[k].SetPosition(0, t1.position);
					sp.edges[k].SetPosition(1, t2.position);
					if (sp.adjac_matrix[i, j] == 2)
					{
					int currentedge=sp.edge_number[i,j];
					sp.line_feedback[currentedge].SetPosition(0, t1.position);
					sp.line_feedback[currentedge].SetPosition(1, t2.position);
					}
					//sp.edges[k].enabled = true;

					
					k++;
				}
			}
		}
	}
}
