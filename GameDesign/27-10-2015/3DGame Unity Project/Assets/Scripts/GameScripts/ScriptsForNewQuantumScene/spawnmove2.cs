﻿using UnityEngine;
using System.Collections;

public class spawnmove2 : MonoBehaviour {

	public spawnItem_new sp1;
	public gameplay play2;
	public gameplay2 play;
	public mode TranquilMode;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (!TranquilMode.modeselected ()  &&!play.lose_flag&&!play2.lose_flag) {
			
			if(sp1.Coins[0].transform.position.z<=-1)//disappearing threshold
			{
				play.disappear_graph();
				sp1.SpawnCoin();
			}
			else{
				change_stars();
				change_edges ();
			}
		}
		
		//t.Translate(orig * Time.deltaTime);
		//t.Rotate(Vector3.forward, 10f * Time.deltaTime);
	}

	void change_stars()
	{
		Vector3 orig;
		Vector3 speed1 = new Vector3 (2, 0, -7);
		//Vector3 speed2 = new Vector3 (-2, 0, -7);
		Vector3 speed3 = new Vector3 (2, -1, -7);
		Vector3 speed4 = new Vector3 (0, -1, -7);
		//Vector3 speed5 = new Vector3 (-2, -1, -7);
		Vector3[] speed={speed1,speed3,speed4};

		int n = Random.Range (0, speed.Length-1);
		
		orig =speed[n];
		
		
		orig.z = -(12-sp1.no_edges/2);//movement of graph
		for (int i = 0; i < sp1.no_stars ; i++) {
			Transform t = sp1.Coins [i].GetComponent<Transform> ();
            Vector3 Transposition = orig * Time.deltaTime * 0.03f;
			t.Translate (Transposition);
            //write a if loop to make the graph disappear after it crosses this virtual line. 
            //Need to find that imaginary line heuristically.. 
		}
	}
	void change_edges()
	{
		int k = 0;
		int sum = 0;
		int i;
		for (i = 0; i < sp1.no_stars - 1; i++)
		{
			for (int j = i + 1; j < sp1.no_stars; j++)
			{
				
				
				if (sp1.adjac_matrix[i, j] >= 1)
				{
					//sp1.adjac_matrix[i, j] = 1;
					//sp1.edges[k].enabled = false;
					Transform t1 = sp1.Coins [i].GetComponent<Transform> ();
					
					Transform t2 = sp1.Coins [j].GetComponent<Transform> ();
					sp1.edges[k].SetPosition(0, t1.position);
					sp1.edges[k].SetPosition(1, t2.position);
					if (sp1.adjac_matrix[i, j] == 2)
					{int currentedge=sp1.edge_number[i,j];
					sp1.line_feedback[currentedge].SetPosition(0, t1.position);
					sp1.line_feedback[currentedge].SetPosition(1, t2.position);
					}
					//sp1.edges[k].enabled = true;
					
					
					k++;
				}
			}
		}
	}
}
