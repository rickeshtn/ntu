﻿using UnityEngine;
using System.Collections;
using Leap;

public class spawnItemNew1 : MonoBehaviour
{
 
	public PanelManager GameRestart;
	public NewLeapInterface leapball;
	public Transform[] SpawnPoints;
    public GameObject[] flyStarsAnim;
	public GameObject[] Coins;
	public LineRenderer[] edges, line_feedback;
	//public Light[] starLight;
	public Renderer[] starColor;
	//public LineRenderer feedback;
	//public GameObject[] wormhole;
	public scoreInfo ss;
	public timeInfo tt;
	//public MovieTexture explosion;
	//public GameObject GameExplosion;
	public lvlNumber levelNumb;
	public GUIText LevelNo;
	public GameObject LevelDisp;
	public GameObject TimeDisp;
	public AudioSource StarAudio;
	public mode TranquilMode; // Dear future developer, set this to false and change it in start script based upon the output from previous scene. 
	//If user had reached the game play through tranquail mode set it to true. 
	//For now I(Rickesh) am setting it to true. As I want to test the tranquail mode. 
	
	
	public float intTime = 10f;
	public int[] spawnIndex;
	public int no_stars, no_edges;
	public int[,] adjac_matrix;
	public int[,] edge_number;
	public bool graph_done = false;
	public bool lose_flag=false;
	bool eggIsOn = false;
	int egg_num;
	int time1, time2, time3, win_time;
	int old_star, current_edge;
	public bool first_flag = true;
	bool win_flag = false;
	Vector3 orig_sc;
	public bool create_graph=false;
	int amp=0;
	
	//Color orig_color;
	//public float spawnTime=2f;
	//public int no_stars=5;
	


    //Color orig_color;
    //public float spawnTime=2f;
    //public int no_stars=5;

    // Use this for initialization
	void Start()
	{
		if(!TranquilMode.modeselected())
		{
			//((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
			int i;
			//LevelDisp = GameObject.Find("LevelNumber");
			LevelDisp.gameObject.transform.localPosition = new Vector3(-350,-350,0);
			//InvokeRepeating ("SpawnCoin", intTime, spawnTime);
			Invoke("SpawnCoin", intTime);
			Renderer rr;
			for (i=0; i<no_stars; i++) {
				rr = Coins [i].GetComponent<Renderer> ();
				rr.material.shader = Shader.Find ("Specular");
				rr.material.SetColor ("_Color", Color.red);
			}
			Transform ttt = Coins[0].GetComponent<Transform>();
			orig_sc = ttt.localScale;
		}
		else
		{
			int i;
			//InvokeRepeating ("SpawnCoin", intTime, spawnTime);
			Invoke("TranqSpawnCoin", intTime);
			Renderer rr;
			LevelNo.text = levelNumb.levelNum().ToString();
			TimeDisp.gameObject.transform.localPosition = new Vector3(-500, -500, 0);
			
			for (i = 0; i < no_stars; i++)
			{
				rr = Coins[i].GetComponent<Renderer>();
				rr.material.shader = Shader.Find("Specular");
				rr.material.SetColor("_Color", Color.red);
			}
			Transform ttt = Coins[0].GetComponent<Transform>();
			orig_sc = ttt.localScale;
			
		}
		
	}


    // Update is called once per frame
    void Update()
    {
       
    }


    void TranqSpawnCoin()
    {

        print("In Update at TranquilMode TranqSpawnCoin");
        //int[] spawnIndex;// set the index of arr randomly
        int i, j, k, t;
        int max;
        bool flag = false;
        graph_done = false;

        //if ((tt.allowedTime * 0.7 < tt.currentTime) && (TranquilMode == false))
        //{
        //    no_stars = Random.Range(4, 7);//number of stars}
        //}
        //else
        //{
        no_stars = Random.Range(5,8);
        //}
        //no_stars = 3;

        max = (no_stars - 1) / 2 * no_stars;
        if (max > 16)
            max = 16;
        no_edges = Random.Range(no_stars + 1, max);
        //no_edges = 3;
        /*//Debug.Log ("no_stars");
        //Debug.Log (no_stars);
        //Debug.Log ("no_edge");
        //Debug.Log (no_edges);*/

        spawnIndex = new int[no_stars];
        //create adjacent matrix
        adjac_matrix = new int[no_stars, no_stars];
        int[] degree = new int[no_stars];
        int node1, node2, no_odd, e, f;
        int[] comp = new int[no_stars];
        for (i = 0; i < no_stars; i++)
        {
            degree[i] = 0;
            comp[i] = 0;
            for (j = 0; j < no_stars; j++)
            {
                adjac_matrix[i, j] = 0;
            }
        }
        k = 0;
        node1 = Random.Range(0, no_stars - 1);
        node2 = Random.Range(0, no_stars - 1);
        while (node2 == node1)
        {
            node2 = Random.Range(0, no_stars - 1);
        }
        degree[node1] += 1;
        degree[node2] += 1;
        adjac_matrix[node1, node2] += 1;
        adjac_matrix[node2, node1] += 1;
        comp[node1] = 1;
        comp[node2] = 1;
        k++;
        //create a connected graph
        for (i = 0; i < no_stars - 2; i++)
        {
            for (j = 0; j < no_stars; j++)
            {
                if (comp[j] == 1 && degree[j] < no_stars / 2)
                {
                    node1 = j;
                    break;
                }
            }
            for (j = 0; j < no_stars; j++)
            {
                if (comp[j] == 0 && degree[j] < no_stars / 2)
                {
                    node2 = j;
                    break;
                }
            }
            degree[node1] += 1;
            degree[node2] += 1;
            adjac_matrix[node1, node2] += 1;
            adjac_matrix[node2, node1] += 1;
            comp[node1] = 1;
            comp[node2] = 1;
            k++;
        }
        // add edges into the connected graph
        no_odd = 0;
        for (i = 0; i < no_stars; i++)
        {
            if (degree[i] % 2 != 0)
            {
                no_odd++;
            }
        }

        //		//Debug.Log ("no_odd");
        //		//Debug.Log (no_odd);
        //		

        // step1
        while (no_odd > 2)
        {


            node1 = Random.Range(0, no_stars - 1);
            while (degree[node1] % 2 == 0)
            {
                node1 = Random.Range(0, no_stars - 1);
            }

            t = 0;

            for (i = 0; i < no_stars; i++)
            {
                if (degree[i] % 2 != 0 && node1 != i && adjac_matrix[node1, i] == 0)
                {
                    node2 = i;
                    break;
                }

            }
            if (i == no_stars)
            {
                for (e = 0; e < no_stars; e++)
                {
                    if (degree[e] / 2 == 0)
                    {
                        for (f = 0; f < no_stars; f++)
                        {
                            if (degree[f] / 2 != 0 && adjac_matrix[f, e] == 0)
                            {
                                node1 = f;
                                node2 = e;
                                //if(adjac_matrix[node1,node2]>0)//Debug.Log ("repeat in step2 of step1");
                                //if(node1==node2)//Debug.Log ("loop in  step2 of step 1");

                                degree[node1] += 1;
                                degree[node2] += 1;
                                adjac_matrix[node1, node2] += 1;
                                adjac_matrix[node2, node1] += 1;
                                k++;
                                break;

                            }

                        }
                        if (f < no_stars)
                            break;
                    }

                }
                //if(e==no_stars) //Debug.Log ("no action in step 2 of step 1")	;	

            }
            else
            {
                //if(adjac_matrix[node1,node2]>0)//Debug.Log ("repeat in step1");
                //if(node1==node2)//Debug.Log ("loop in final step1");
                degree[node1] += 1;
                degree[node2] += 1;
                adjac_matrix[node1, node2] = 1;
                adjac_matrix[node2, node1] = 1;
                k++;
                no_odd -= 2;
                if (no_edges - 1 < k)
                    no_edges = k + 1;

            }
        }
        //		//Debug.Log ("no_edge");
        //		//Debug.Log (no_edges);

        //step2
        while ((no_edges - 1) > k)
        {

            //choose one odd one even
            for (i = 0; i < no_stars; i++)
            {
                if (degree[i] / 2 == 0)
                {
                    for (j = 0; j < no_stars; j++)
                    {
                        if (degree[j] / 2 != 0 && adjac_matrix[j, i] == 0)
                        {
                            node1 = i;
                            node2 = j;
                            /*if (adjac_matrix [node1, node2] > 0)
                                //Debug.Log ("repeat in step2");
                            if (node1 == node2)
                                //Debug.Log ("loop in  step2");*/

                            degree[node1] += 1;
                            degree[node2] += 1;
                            adjac_matrix[node1, node2] = 1;
                            adjac_matrix[node2, node1] = 1;
                            k++;
                            break;

                        }

                    }
                    if (j < no_stars)
                        break;
                }

            }
            if (i == no_stars)
            {
                no_edges = k + 1;
                ////Debug.Log ("no action in step 2"); //Debug.Log ("no_edges");//Debug.Log (no_edges);
            }
        }



        //step3:
        t = 0;
        for (i = 0; i < no_stars; i++)
        {
            if (degree[i] % 2 != 0 && t > 0)
            {
                node2 = i;
                break;
            }
            if (degree[i] % 2 != 0 && t == 0)
            {
                node1 = i;
                t++;
            }
        }
        if (adjac_matrix[node1, node2] > 0)
            Debug.Log("repeat in final step");
        else
            k++;
        //if(node1==node2)//Debug.Log ("loop in final step");
        no_odd -= 2;
        degree[node1] += 1;
        degree[node2] += 1;
        adjac_matrix[node1, node2] = 1;
        adjac_matrix[node2, node1] = 1;
        no_edges = k;
        //display stars
        for (i = 0; i < no_stars; i++)
        {
            spawnIndex[i] = Random.Range(0, SpawnPoints.Length - 1);
            while (true)
            {
                for (j = 0; j < i; j++)
                {
                    if (spawnIndex[j] == spawnIndex[i])
                    {
                        flag = true;
                        break;
                    }
                }

                if (flag)
                {
                    spawnIndex[i] = Random.Range(0, SpawnPoints.Length - 1);//!!!!bug1
                    flag = false;
                }
                else
                    break;
            }

            Coins[i].SetActive(true);
            //Instantiate (Coins [i], SpawnPoints [spawnIndex [i]].position, SpawnPoints [spawnIndex [i]].rotation);
            Coins[i].transform.position = SpawnPoints[spawnIndex[i]].position;
            ParticleAnimator starFireColor = starColor[i].GetComponent<ParticleAnimator>();
            Color[] selColors = starFireColor.colorAnimation;
            selColors[0] = Color.yellow;
            selColors[1] = Color.yellow;
            selColors[2] = Color.yellow;
            selColors[3] = Color.yellow;
            selColors[4] = Color.yellow;
            starFireColor.colorAnimation = selColors;
            //starColor[i].material.color = new Color(1, 1, 1);
            //starLight[i].color = new Color(1, 1, 1);
            //Transform tc= Coins[i].GetComponent<Transform>();
            //tc.position=SpawnPoints[spawnIndex[i]].position;

        }

        //display  edges


        k = 0;
        int sum = 0;
        for (i = 0; i < no_stars - 1; i++)
        {
            for (j = i + 1; j < no_stars; j++)
            {

                if (adjac_matrix[i, j] > 1)
                {
                    sum++;
                    //					//Debug.Log ("repeat");//Debug.Log(sum);
                }
                if (adjac_matrix[i, j] >= 1)
                {
                    adjac_matrix[i, j] = 1;
                    edges[k].enabled = true;
                    edges[k].SetPosition(0, SpawnPoints[spawnIndex[i]].position);
                    edges[k].SetPosition(1, SpawnPoints[spawnIndex[j]].position);
                    edges[k].SetWidth(0.2f, 0.2f);
                    //edges[k].SetColors(Color.white, Color.white);
                    //edges [k].material.SetColor("_SpecColor",Color.red);

                    k++;
                }
            }
        }
        //		//Debug.Log ("k2");
        //		//Debug.Log (k);

        for (i = 0; i < no_stars; i++)
        {
            if (adjac_matrix[i, i] != 0)
                Debug.Log("diagnal");


        }
        first_flag = true;
        // feedback.enabled = true;
        print("no_stars" + no_stars);
        print("no_edges" + no_edges);

        if (TranquilMode == false) //for now it is false in Quantum mode it was not there. 
            time3 = tt.allowedTime; //The actual syntax here is if(TranquilMode == false)
        //disappear_edge 
        /*for (i=0; i<edges.Length; i++) {
            edges [i].enabled = false;
        }*/
		amp = 0;

    }

    public void SpawnCoin()
    {

		create_graph = false;
		//int[] spawnIndex;// set the index of arr randomly
		int i, j, k, t;
		int max;
		bool flag = false;
		graph_done = false;

		if ((tt.allowedTime * 0.8 < tt.currentTime)) {
			no_stars = Random.Range (3, 4);//number of stars}
			//no_stars=6;
		} 
		else {
			if ((tt.allowedTime * 0.6 < tt.currentTime))
			{
				no_stars = Random.Range (4, 5);}
				else{
				if ((tt.allowedTime * 0.4 < tt.currentTime))
					no_stars = Random.Range (4, 6);
				else 
					if (tt.allowedTime * 0.2 < tt.currentTime)
				{
					no_stars = 3;
				}
				else{
					no_stars = 3;
				}
			}
		    }

		//no_stars = 6;

		max = (no_stars - 1) / 2 * no_stars;
		if (max > 16)
			max = 16;
		no_edges = Random.Range (no_stars + 1, max);
		//no_edges = 3;
		/*//Debug.Log ("no_stars");
        //Debug.Log (no_stars);
        //Debug.Log ("no_edge");
        //Debug.Log (no_edges);*/
		no_edges=number_spawn (no_stars,no_edges);
        for (int l = 0; l < 7; l++)
        {
            //flyStarsAnim[l].SetActive(!(flyStarsAnim[l].active));
            flyStarsAnim[l].SetActive(false);
        }
	}
	int number_spawn(int no_stars, int no_edges)
	{
		create_graph = false;
		//int[] spawnIndex;// set the index of arr randomly
		int i, j, k, t;
		int max;
		bool flag = false;
		graph_done = false;
        spawnIndex = new int[no_stars];
        //create adjacent matrix
        adjac_matrix = new int[no_stars, no_stars];
		edge_number = new int[no_stars, no_stars];
        int[] degree = new int[no_stars];
        int node1, node2, no_odd, e, f;
        int[] comp = new int[no_stars];
        for (i = 0; i < no_stars; i++)
        {
            degree[i] = 0;
            comp[i] = 0;
            for (j = 0; j < no_stars; j++)
            {
                adjac_matrix[i, j] = 0;
				edge_number[i,j]=0;
            }
        }
        k = 0;
        node1 = Random.Range(0, no_stars - 1);
        node2 = Random.Range(0, no_stars - 1);
        while (node2 == node1)
        {
            node2 = Random.Range(0, no_stars - 1);
        }
        degree[node1] += 1;
        degree[node2] += 1;
        adjac_matrix[node1, node2] += 1;
        adjac_matrix[node2, node1] += 1;
        comp[node1] = 1;
        comp[node2] = 1;
        k++;
        //create a connected graph
        for (i = 0; i < no_stars - 2; i++)
        {
            for (j = 0; j < no_stars; j++)
            {
                if (comp[j] == 1 && degree[j] < no_stars / 2)
                {
                    node1 = j;
                    break;
                }
            }
            for (j = 0; j < no_stars; j++)
            {
                if (comp[j] == 0 && degree[j] < no_stars / 2)
                {
                    node2 = j;
                    break;
                }
            }
            degree[node1] += 1;
            degree[node2] += 1;
            adjac_matrix[node1, node2] += 1;
            adjac_matrix[node2, node1] += 1;
            comp[node1] = 1;
            comp[node2] = 1;
            k++;
        }
        // add edges into the connected graph
        no_odd = 0;
        for (i = 0; i < no_stars; i++)
        {
            if (degree[i] % 2 != 0)
            {
                no_odd++;
            }
        }

        //		//Debug.Log ("no_odd");
        //		//Debug.Log (no_odd);
        //		

        // step1
        while (no_odd > 2)
        {


            node1 = Random.Range(0, no_stars - 1);
            while (degree[node1] % 2 == 0)
            {
                node1 = Random.Range(0, no_stars - 1);
            }

            t = 0;

            for (i = 0; i < no_stars; i++)
            {
                if (degree[i] % 2 != 0 && node1 != i && adjac_matrix[node1, i] == 0)
                {
                    node2 = i;
                    break;
                }

            }
            if (i == no_stars)
            {
                for (e = 0; e < no_stars; e++)
                {
                    if (degree[e] / 2 == 0)
                    {
                        for (f = 0; f < no_stars; f++)
                        {
                            if (degree[f] / 2 != 0 && adjac_matrix[f, e] == 0)
                            {
                                node1 = f;
                                node2 = e;
                                //if(adjac_matrix[node1,node2]>0)//Debug.Log ("repeat in step2 of step1");
                                //if(node1==node2)//Debug.Log ("loop in  step2 of step 1");

                                degree[node1] += 1;
                                degree[node2] += 1;
                                adjac_matrix[node1, node2] += 1;
                                adjac_matrix[node2, node1] += 1;
                                k++;
                                break;

                            }

                        }
                        if (f < no_stars)
                            break;
                    }

                }
                //if(e==no_stars) //Debug.Log ("no action in step 2 of step 1")	;	

            }
            else
            {
                //if(adjac_matrix[node1,node2]>0)//Debug.Log ("repeat in step1");
                //if(node1==node2)//Debug.Log ("loop in final step1");
                degree[node1] += 1;
                degree[node2] += 1;
                adjac_matrix[node1, node2] = 1;
                adjac_matrix[node2, node1] = 1;
                k++;
                no_odd -= 2;
                if (no_edges - 1 < k)
                    no_edges = k + 1;

            }
        }
        //		//Debug.Log ("no_edge");
        //		//Debug.Log (no_edges);

        //step2
        while ((no_edges - 1) > k)
        {

            //choose one odd one even
            for (i = 0; i < no_stars; i++)
            {
                if (degree[i] / 2 == 0)
                {
                    for (j = 0; j < no_stars; j++)
                    {
                        if (degree[j] / 2 != 0 && adjac_matrix[j, i] == 0)
                        {
                            node1 = i;
                            node2 = j;
                            /*if (adjac_matrix [node1, node2] > 0)
                                //Debug.Log ("repeat in step2");
                            if (node1 == node2)
                                //Debug.Log ("loop in  step2");*/

                            degree[node1] += 1;
                            degree[node2] += 1;
                            adjac_matrix[node1, node2] = 1;
                            adjac_matrix[node2, node1] = 1;
                            k++;
                            break;

                        }

                    }
                    if (j < no_stars)
                        break;
                }

            }
            if (i == no_stars)
            {
                no_edges = k + 1;
                ////Debug.Log ("no action in step 2"); //Debug.Log ("no_edges");//Debug.Log (no_edges);
            }
        }



        //step3:
        t = 0;
        for (i = 0; i < no_stars; i++)
        {
            if (degree[i] % 2 != 0 && t > 0)
            {
                node2 = i;
                break;
            }
            if (degree[i] % 2 != 0 && t == 0)
            {
                node1 = i;
                t++;
            }
        }
        if (adjac_matrix[node1, node2] > 0)

			//Debug.log
	
            Debug.Log("repeat in final step");
        else
            k++;
        //if(node1==node2)//Debug.Log ("loop in final step");
        no_odd -= 2;
        degree[node1] += 1;
        degree[node2] += 1;
        adjac_matrix[node1, node2] = 1;
        adjac_matrix[node2, node1] = 1;
        no_edges = k;
        //display stars
        for (i = 0; i < no_stars; i++)
        {
            spawnIndex[i] = Random.Range(0, SpawnPoints.Length - 1);
            while (true)
            {
                for (j = 0; j < i; j++)
                {
                    if (spawnIndex[j] == spawnIndex[i])
                    {
                        flag = true;
                        break;
                    }
                }

                if (flag)
                {
                    spawnIndex[i] = Random.Range(0, SpawnPoints.Length - 1);//!!!!bug1
                    flag = false;
                }
                else
                    break;
            }

            Coins[i].SetActive(true);
            //Instantiate (Coins [i], SpawnPoints [spawnIndex [i]].position, SpawnPoints [spawnIndex [i]].rotation);
            Coins[i].transform.position = SpawnPoints[spawnIndex[i]].position;
            ParticleAnimator starFireColor = starColor[i].GetComponent<ParticleAnimator>();
            Color[] selColors = starFireColor.colorAnimation;
            selColors[0] = Color.yellow;
            selColors[1] = Color.yellow;
            selColors[2] = Color.yellow;
            selColors[3] = Color.yellow;
            selColors[4] = Color.yellow;
            starFireColor.colorAnimation = selColors;
            //starColor[i].material.color = new Color(1, 1, 1);
            //starLight[i].color = new Color(1, 1, 1);
            //Transform tc= Coins[i].GetComponent<Transform>();
            //tc.position=SpawnPoints[spawnIndex[i]].position;

        }

        //display  edges


        k = 0;
        int sum = 0;
        for (i = 0; i < no_stars - 1; i++)
        {
            for (j = i + 1; j < no_stars; j++)
            {

                if (adjac_matrix[i, j] > 1)
                {
                    sum++;
                    //					//Debug.Log ("repeat");//Debug.Log(sum);
                }
                if (adjac_matrix[i, j] >= 1)
                {
                    adjac_matrix[i, j] = 1;
					edge_number[i,j]=k;
					edge_number[j,i]=k;

                    edges[k].enabled = true;

                    edges[k].SetPosition(0, SpawnPoints[spawnIndex[i]].position);
                    edges[k].SetPosition(1, SpawnPoints[spawnIndex[j]].position);
                    edges[k].SetWidth(0.2f, 0.2f);
					edges[k].SetColors(Color.green, Color.green);

                    //edges[k].SetColors(Color.white, Color.white);
                    //edges [k].material.SetColor("_SpecColor",Color.red);

                    k++;
                }
            }
        }
        //		//Debug.Log ("k2");
        //		//Debug.Log (k);

        for (i = 0; i < no_stars; i++)
        {
            if (adjac_matrix[i, i] != 0)
                Debug.Log("diagnal");


        }
        first_flag = true;
        // feedback.enabled = true;
        //print("no_stars" + no_stars);
        //print("no_edges" + no_edges);

        time3 = tt.allowedTime;
		create_graph = true;
		lose_flag = false;
		return k;
        //disappear_edge 
        /*for (i=0; i<edges.Length; i++) {
            edges [i].enabled = false;
        }*/
		amp = 0;

    }

}
