﻿using UnityEngine;
using System.Collections;

public class NewMouseInterface : MonoBehaviour
{
    public GameObject capsule;
    public Transform Sphere;
    private float ScaleFactor = 1.15f;
    private Vector3 cameraRelative = new Vector3();
    public spawnItemNew1 SpawnGraph1;
    public spawnItem_new SpawnGraph2;
    public int click_ball = -1;
    public int click_ball2 = -1;
    private static Vector3 GameobjectLoc = new Vector3(-370, -250, 0);
    // Use this for initialization

    void Start()
    {
        Cursor.visible = false;
        //Cursor.visible = false;
        //Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.visible = false;
        //Cursor.SetCursor(null, Vector2.zero, cursorMode);
        //Ray toMouse = new Ray();
        //toMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
        //RaycastHit rhInfo;
        //bool didHit = Physics.Raycast(toMouse, out rhInfo, 500.0f);
        //float range = 1.0f;
        //if (didHit)
        //{
        //    for(int i=0;i<spawnPoints.no_stars;i++)
        //    {
        //        Sphere= spawnPoints.Coins[i].GetComponent<Transform>();
        //        //print("PosLeap " + PosLeap + "FixedSphere  " + Sphere.position + "Camera Ralative " + cameraRelative);

        //         bool test = ((Sphere.position.x - range < rhInfo.collider.transform.position.x)
        //                     && (Sphere.position.x + range > rhInfo.collider.transform.position.x))
        //                        && ((Sphere.position.y - range < rhInfo.collider.transform.position.y)
        //                            && (Sphere.position.y + range > rhInfo.collider.transform.position.y));
        //        if (test)
        //        {
        //            print("Success and click on ball"+i);
        //            click_ball=i;
        //            Transform tro = spawnPoints.Coins[i].GetComponent<Transform> ();
        //            //tro.Rotate (Vector3.left, 90);
        //            /*Vector3 n= new Vector3(20,20,20);
        //        Transform T=Sphere.GetComponent<Transform>();
        //        T.position=n;*/

        //        }
        //    }
        //}


        Transform Cam = Camera.main.transform;
        Vector3 PosMouse = new Vector3();
        if (Input.mousePresent)
        {
            Vector3 position = Input.mousePosition;
            transform.localPosition = new Vector3((GameobjectLoc.x + position.x) * ScaleFactor, (GameobjectLoc.y + position.y) * ScaleFactor, 0);
            PosMouse = transform.localPosition;
            cameraRelative = Cam.InverseTransformDirection(transform.position);
        }
        float range = 0.3f;
        for (int i = 0; i < SpawnGraph1.no_stars; i++)
        {
            Sphere = SpawnGraph1.Coins[i].GetComponent<Transform>();

            //print("PosLeap " + PosLeap + "FixedSphere  " + Sphere.position + "Camera Ralative " + cameraRelative);
            bool test = ((cameraRelative.x - range < Sphere.position.x)
                         && (cameraRelative.x + range > Sphere.position.x))
                            && ((cameraRelative.y - range < Sphere.position.y)
                                && (cameraRelative.y + range > Sphere.position.y));


            if (test)
            {
                //print("Success and click on ball" + i);
                click_ball = i;
                Transform tro = SpawnGraph1.Coins[i].GetComponent<Transform>();
                //tro.Rotate (Vector3.left, 90);
                /*Vector3 n= new Vector3(20,20,20);
            Transform T=Sphere.GetComponent<Transform>();
            T.position=n;*/
            }
        }

        for (int i = 0; i < SpawnGraph2.no_stars; i++)
        {
            Sphere = SpawnGraph2.Coins[i].GetComponent<Transform>();
            bool testGraph2 = ((cameraRelative.x - range < Sphere.position.x)
                                && (cameraRelative.x + range > Sphere.position.x))
                                    && ((cameraRelative.y - range < Sphere.position.y)
                                        && (cameraRelative.y + range > Sphere.position.y));

            if (testGraph2)
            {
                click_ball2 = i;
                Transform tro = SpawnGraph2.Coins[i].GetComponent<Transform>();
            }
        }
    }
}
