﻿using UnityEngine;
using System.Collections;

public class camera_move : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 orig = new Vector3 (0, 0, -1);
		
		Transform t = GetComponent<Transform> ();
		t.Translate (orig * Time.deltaTime);
		
	}
}