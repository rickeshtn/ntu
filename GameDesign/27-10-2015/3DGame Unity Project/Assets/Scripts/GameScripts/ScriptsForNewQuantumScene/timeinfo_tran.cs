﻿using UnityEngine;
using System.Collections;

public class timeinfo_tran : MonoBehaviour {

	private GUIText textfield;
	public  int allowedTime = 80;
	public int currentTime ;
	public spawnItem_tran spawnObj;
	public play_tran playObj;
	//public gameplay2 playObj2;
	//public mode TranMode;
	// Use this for initialization
	void Start () {
		currentTime = allowedTime;
		textfield = GameObject.Find("TimeInfo").GetComponent<GUIText>();
		UpdateTimerText();
		// start the timer ticking
		StartCoroutine(TimerTick());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	IEnumerator TimerTick()
	{
		// while there are seconds left
		
		while (currentTime > 0 && !playObj.lose_flag) {
			// wait for 1 second
			yield return new WaitForSeconds(1);
			// reduce the time
			currentTime--;
			//Debug.Log("counting...");
			UpdateTimerText ();
		}
		
		if (currentTime == 0) {
			playObj.loseGraph ();
		}
	}
	
	void UpdateTimerText()
	{
		// update the textfield
		textfield.text = currentTime.ToString();
	}
}
