﻿using UnityEngine;
using System.Collections;

public class play_tran : MonoBehaviour {

	// Use this for initialization
	public spawnItem_tran spwn;
	//public gameplay2 thread2;
	//public spawnItem_new spwn2;
	public PanelManager GameRestart;
	public LeapInterFace leapball;
	//public Transform[] spwn.SpawnPoints;
	//public GameObject[] spwn.Coins;
	//public LineRenderer[] edges, spwn.line_feedback;
	//public Light[] spwn.starLight;
	//public Renderer[] spwn.starColor;
	//public LineRenderer feedback;
	//public GameObject[] wormhole;
	public scoreInfo ss;
	public timeinfo_tran tt;
	public MovieTexture explosion;
	public GameObject GameExplosion;
	public lvlNumber levelNumb;
	public GUIText LevelNo;
	public GameObject LevelDisp;
	public GameObject TimeDisp;
	public AudioSource StarAudio;
	public mode TranquilMode; // Dear future developer, set this to false and change it in start script based upon the output from previous scene. 
	//If user had reached the game play through tranquail mode set it to true. 
	//For now I(Rickesh) am setting it to true. As I want to test the tranquail mode. 
	
	
	public float intTime = 10f;
	//public int[] spwn.spawnIndex;
	//public int spwn.no_stars, spwn.no_edges;
	//public int[,] spwn.adjac_matrix;
	public bool graph_done = false;
	public bool lose_flag=false;
	bool eggIsOn = false;
	int egg_num;
	int time1, time2, time3, win_time;
	int old_star, current_edge;
	//fibool spwn.first_flag = true;
	public bool win_flag = false;
	Vector3 orig_sc;
	public bool create_graph=false;
	int amp=0;
	
	
	void Start()
	{
		if(!TranquilMode.modeselected())
		{
			//((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
			int i;
			//LevelDisp = GameObject.Find("LevelNumber");
			LevelDisp.gameObject.transform.localPosition = new Vector3(-350,-350,0);
			//InvokeRepeating ("SpawnCoin", intTime, spawnTime);
			Invoke("SpawnCoin", intTime);
			Renderer rr;
			for (i=0; i<spwn.no_stars; i++) {
				rr = spwn.Coins [i].GetComponent<Renderer> ();
				rr.material.shader = Shader.Find ("Specular");
				rr.material.SetColor ("_Color", Color.red);
			}
			Transform ttt = spwn.Coins[0].GetComponent<Transform>();
			orig_sc = ttt.localScale;
		}
		else
		{
			int i;
			//InvokeRepeating ("SpawnCoin", intTime, spawnTime);
			Invoke("TranqSpawnCoin", intTime);
			Renderer rr;
			LevelNo.text = levelNumb.levelNum().ToString();
			TimeDisp.gameObject.transform.localPosition = new Vector3(-350, -350, 0);
			
			for (i = 0; i < spwn.no_stars; i++)
			{
				rr = spwn.Coins[i].GetComponent<Renderer>();
				rr.material.shader = Shader.Find("Specular");
				rr.material.SetColor("_Color", Color.red);
			}
			Transform ttt = spwn.Coins[0].GetComponent<Transform>();
			orig_sc = ttt.localScale;
			
		}
		
	}
	
	// Update is called once per frame
	void Update()
	{
		if (!TranquilMode.modeselected())
		{
			Vector3 orig = new Vector3(0, 5, 10);
			Vector3 haha2 = new Vector3(90, 90, 90);
			Vector3 haha = new Vector3(3, 3, 3);
			{
				if (win_flag )
				{
					
					if (tt.currentTime <= (win_time - 2f) )
					{
						win_flag = false;
						//thread2.win_flag=false;
						
						for (int i = 0; i < spwn.Coins.Length; i++)
						{
							Transform LALA = spwn.Coins[i].GetComponent<Transform>();
							LALA.localScale = orig_sc;
							
							spwn.Coins[i].SetActive(false);
						}
						
						// NEED TO ADD: clear animation as a feedback of completing a graph
						spwn.first_flag = true;
						spwn.SpawnCoin();
					}
					else
					{
						//float freq=0.5f;
						//float unit=0f;
						//amp=0;
						Vector3 vv=new Vector3(1,0,0);
						Vector3 vvy= new Vector3(0,Mathf.Pow(2,(amp/2)),0);
						amp++;
						for (int i = 0; i < spwn.no_stars; i++)
						{
							if(i%2==0)
							{orig.y=5;}
							else{
								orig.y=-5;
							}
							
							Transform tcoin = spwn.Coins[i].GetComponent<Transform>();
							tcoin.Translate(orig * Time.deltaTime);
							//!!!!!!!!!!!!!!!!!scale
							//tcoin.Rotate(Vector3.forward, 10f * Time.deltaTime);
							//tcoin.position = tcoin.position+vv* Time.deltaTime;
							//tcoin.position = tcoin.position+vvy * Time.deltaTime;
							
							//unit += Time.deltaTime;
							
							//tcoin.localScale = tcoin.localScale + haha * Time.deltaTime;
							
						}
					}
				}
				else
				{
					if ( tt.currentTime % 3 == 0&&!eggIsOn && !spwn.first_flag && spwn.no_stars >= 3   )
					{
						disp_easterEgg();
						//((time2 - tt.currentTime) >= 1)
						//tt.currentTime % 2 == 0 &&
						//tt.currentTime % 1 == 0 
						//!eggIsOn &&
						//!spwn.first_flag && spwn.no_stars >= 3 &&
						
						//print ("display easter egg!!!!!!!!");
						
					}
					
					cancel_easterEgg();
					check_edge();
				}
			}
		}
		else
		{
			Vector3 orig = new Vector3(0, 0, 12);
			Vector3 haha2 = new Vector3(90, 90, 90);
			Vector3 haha = new Vector3(3, 3, 3);
			TranqCheckedge();
		}
	}
	void disp_easterEgg()
	{
		
		////print ("!!!!!!!!!!!!!!!!!!!!!!!!!!haha" + spwn.first_flag);
		int no_left_line = 0;
		bool find_flag = false;
		for (int i = 0; i < spwn.no_stars; i++)
		{
			no_left_line = 0;
			
			for (int j = 0; j < spwn.no_stars; j++)
			{
				if (spwn.adjac_matrix[i, j] == 1)
				{
					no_left_line++;
					if (no_left_line >= 1 && i != old_star && spwn.adjac_matrix[i, old_star] == 0)
					{
						find_flag = true;
						egg_num = i;
						break;
					}
				}
			}
			if (find_flag)
				break;
		}
		Vector3 sc = new Vector3(2, 2, 2);
		if (find_flag)
		{
			eggIsOn = true;
			time1 = tt.currentTime;
			
			
			Transform eggt = spwn.Coins[egg_num].GetComponent<Transform>();
			eggt.localScale = sc;
			spwn.starColor[egg_num].material.color = new Color(1, 0, 0);
			spwn.starLight[egg_num].color = new Color(1, 0, 0);
		}
		
	}
	void cancel_easterEgg()
	{
		
		if (eggIsOn)
		{
			
			time2 = tt.currentTime;
			time3 = time1 - time2;
			if (time3 >= 2)
			{
				////print ("haha"+tt.currentTime);
				eggIsOn = false;
				/*Renderer rend = spwn.Coins [egg_num].GetComponent<Renderer> ();
                rend.material.shader = Shader.Find ("Specular");
                rend.material.SetColor ("_Color",Color.red);*/
				Transform eggt = spwn.Coins[egg_num].GetComponent<Transform>();
				eggt.localScale = orig_sc;
				spwn.starColor[egg_num].material.color = new Color(1, 1, 1);
				spwn.starLight[egg_num].color = new Color(1, 1, 1);
				time2=tt.currentTime;
			}
		}
	}
	void check_edge()
	{
		
		int numMouse = clickOn_ball();
		//int numMouse = newclickOn_ball();
		int numLeap = LeapClickOn_ball();
		int num = -1;
		if ((numMouse >= 0) || (numLeap >= 0))
		{
			if (numMouse >= 0)
				num = numMouse;
			else
			{
				num = numLeap;
				////Debug.Log("numLeap ");
				////Debug.Log(numLeap);
			}
		}
		if (num >= 0)
		{
			
			if (spwn.first_flag)
			{
				//current_edge = 0;
				old_star = num;
				spwn.first_flag = false;
				//print("start point" + old_star);
				spwn.starLight[num].color = new Color(0, 1, 0);
				spwn.starColor[num].material.color = new Color(0, 1, 0);
				StarAudio.Play();
				//spwn.edges[current_edge].SetWidth(0.5f, 0.5f);
				//spwn.edges[current_edge].SetColors(Color.white, Color.white);
			}
			else
			{
				if (spwn.adjac_matrix[num, old_star] == 1)
				{
					if (eggIsOn && num == egg_num)
					{
						if (tt.currentTime < time1) time1 = tt.currentTime;
						
						winGraph();
						//thread2.winGraph();
						
						
					}
					else
					{
						//////Debug.Log("got one edge");
						spwn.adjac_matrix[num, old_star] = 2;
						spwn.adjac_matrix[old_star, num] = 2;
						//change color
						current_edge=spwn.edge_number[num,old_star];
						spwn.line_feedback[current_edge].SetPosition(0, spwn.Coins[old_star].transform.position);
						spwn.line_feedback[current_edge].SetPosition(1, spwn.Coins[num].transform.position);
						spwn.line_feedback[current_edge].SetWidth(0.6f, 0.6f);
						spwn.line_feedback[current_edge].enabled = true;
						//spwn.line_feedback[current_edge].enable=true;
						//LineRenderer lineRenderer = spwn.edges[current_edge].AddComponent<LineRenderer>();
						//spwn.edges[current_edge].material = new Material(Shader.Find("SelfIllumAlpha"));
						//spwn.edges[current_edge].SetColors(Color.green, Color.green);
						//spwn.edges[current_edge].SetPosition(0, spwn.SpawnPoints[spwn.spawnIndex[old_star]].position);
						// spwn.edges[current_edge].SetPosition(1, spwn.SpawnPoints[spwn.spawnIndex[num]].position);
						//spwn.edges[current_edge].SetWidth(0.6f, 0.6f);
						
						// spwn.edges[current_edge].SetColors(Color.green, Color.green);
						
						spwn.starLight[num].color = new Color(0, 1, 0);
						spwn.starColor[num].material.color = new Color(0, 1, 0);
						StarAudio.Play();
						//current_edge++;
						//print("current Time " + tt.currentTime + "allowed time " + tt.allowedTime);
						if (check_graph())
						{
							winGraph();
						}
						else if (no_way(num))
						{
							loseGraph();
						}
						else
						{
							old_star = num;
							
						}
						////print("old_star change to "+num);
					}
				}
				else if (spwn.adjac_matrix[num, old_star] == 2)
				{//repeat
					loseGraph();
					////Debug.Log("oops,repeated");
				}
			}
		}
	}
	
	void TranqCheckedge()
	{
		//print("In Update at TranqCheckedge ");
		int numMouse = clickOn_ball();
		int numLeap = LeapClickOn_ball();
		int num = -1;
		if ((numMouse >= 0) || (numLeap >= 0))
		{
			if (numMouse >= 0)
				num = numMouse;
			else
			{
				num = numLeap;
				////Debug.Log("numLeap ");
				////Debug.Log(numLeap);
			}
		}
		if (num >= 0)
		{
			if (spwn.first_flag)
			{
				//current_edge = 0;
				old_star = num;
				spwn.first_flag = false;
				//print("start point" + old_star);
				spwn.starLight[num].color = new Color(0, 1, 0);
				spwn.starColor[num].material.color = new Color(0, 1, 0);
				StarAudio.Play();
				//spwn.edges[current_edge].SetWidth(0.5f, 0.5f);
				//spwn.edges[current_edge].SetColors(Color.white, Color.white);
			}
			else
			{
				if (spwn.adjac_matrix[num, old_star] == 1)
				{
					{
						//////Debug.Log("got one edge");
						spwn.adjac_matrix[num, old_star] = 2;
						spwn.adjac_matrix[old_star, num] = 2;
						//change color
						current_edge=spwn.edge_number[num,old_star];
						spwn.line_feedback[current_edge].SetPosition(0, spwn.SpawnPoints[spwn.spawnIndex[old_star]].position);
						spwn.line_feedback[current_edge].SetPosition(1, spwn.SpawnPoints[spwn.spawnIndex[num]].position);
						spwn.line_feedback[current_edge].SetWidth(0.6f, 0.6f);
						spwn.line_feedback[current_edge].enabled = true;
						//spwn.edges[current_edge].SetPosition(0, spwn.SpawnPoints[spwn.spawnIndex[old_star]].position);
						//spwn.edges[current_edge].SetPosition(1, spwn.SpawnPoints[spwn.spawnIndex[num]].position);
						//spwn.edges[current_edge].SetWidth(0.6f, 0.6f);
						//spwn.edges[current_edge].material = new Material(Shader.Find("SelfIllumAlpha"));
						//spwn.edges[current_edge].SetColors(Color.green, Color.green);
						//spwn.starLight[num].color = new Color(0, 1, 0);
						//spwn.starColor[num].material.color = new Color(0, 1, 0);
						StarAudio.Play();
						//current_edge++;
						
						if (check_graph())
						{
							winGraph();
						}
						else if (no_way(num))
						{
							loseGraph();
						}
						else
						{
							old_star = num;
							
						}
						//spwn.edges.("old_star change to "+num);
					}
				}
				else if (spwn.adjac_matrix[num, old_star] == 2)
				{//repeat
					loseGraph();
					////Debug.Log("oops,repeated");
				}
				
			}
		}
	}
	
	int clickOn_ball()//whether mouse click on a ball
	{
		//if (Input.GetMouseButtonDown(0))
		//{
		Ray toMouse = new Ray();
		toMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
		////Debug.Log("mouse position");
		////Debug.Log(Input.mousePosition);
		////Debug.Log("To Mouse");
		////Debug.Log(toMouse);
		RaycastHit rhInfo;
		bool didHit = Physics.Raycast(toMouse, out rhInfo, 500.0f);
		if (didHit)
		{
			for (int i = 0; i < spwn.no_stars; i++)
			{
				////Debug.Log("spwn.SpawnPoints");
				////Debug.Log("Location "); ////Debug.Log(i);
				////Debug.Log(spwn.SpawnPoints[spwn.spawnIndex[i]].position);
				////Debug.Log("From Mouse ");
				////Debug.Log(rhInfo.collider.transform.position);
				float range = 1.0f;
				bool test = ((spwn.SpawnPoints[spwn.spawnIndex[i]].position.x - range < rhInfo.collider.transform.position.x)
				             && (spwn.SpawnPoints[spwn.spawnIndex[i]].position.x + range > rhInfo.collider.transform.position.x))
					&& ((spwn.SpawnPoints[spwn.spawnIndex[i]].position.y - range < rhInfo.collider.transform.position.y)
					    && (spwn.SpawnPoints[spwn.spawnIndex[i]].position.y + range > rhInfo.collider.transform.position.y));
				
				if (test)
				{//!!use spwn.spawnIndex instead of coins[i]
					//print("CLICK ON BALL" + i);
					return i;
				}
			}
			return -1;
		}
		//	return -1;
		//}
		return -1;
	}
	
	
	int LeapClickOn_ball()
	{
		if (leapball.click_ball >= 0)
		{
			int leapBall_Click = leapball.click_ball;
			//print("SPAWN:click on ball" + leapball.click_ball);
			StarAudio.Play();
			leapball.click_ball = -1;
			return leapBall_Click;
		}
		else
			return -1;
	}
	
	
	bool check_graph()//whether complete a graph
	{
		for (int i = 0; i < spwn.no_stars; i++)
		{
			for (int j = 0; j < spwn.no_stars; j++)
			{
				if (spwn.adjac_matrix[i, j] == 1)
				{// has not been visited
					return false;
				}
			}
		}
		if (eggIsOn && !TranquilMode.modeselected())
		{
			
			
			eggIsOn = false;
			/*Renderer rend = spwn.Coins [egg_num].GetComponent<Renderer> ();
                rend.material.shader = Shader.Find ("Specular");
                rend.material.SetColor ("_Color",Color.red);*/
			Transform eggt = spwn.Coins[egg_num].GetComponent<Transform>();
			eggt.localScale = orig_sc;
			spwn.starColor[egg_num].material.color = new Color(1, 1, 1);
			spwn.starLight[egg_num].color = new Color(1, 1, 1);
		}
		graph_done = true;
		return true;
	}
	
	bool no_way(int a)
	{
		//bool flag = true;
		for (int k = 0; k < spwn.no_stars; k++)
		{
			if (spwn.adjac_matrix[k, a] == 1)
			{
				return false;
			}
		}
		////Debug.Log("oops,no way ");
		return true;
	}
	
	public void loseGraph()
	{
		////Debug.Log("KO ");
		disappear_graph();
		//thread2.disappear_graph ();
		lose_flag = true;
		//GameExplosion.transform.position = new Vector3 (0, 0, 3);
		//GameExplosion.transform.Translate(0.0f, 0.0f, 3.0f);
		//explosion.Play ();
		//		if (explosion.isPlaying ()) {
		//
		//		}
		//if (explosion.isPlaying == true && explosion.duration == 3) {
		//GameExplosion.transform.Translate (13.0f, 0.0f, 3.0f);
		//GameExplosion.transform.position = new Vector3 (13, 0, 3);
		GameRestart.transform.position = new Vector3(0, 0, 3);
		//		if(GameRestart.)
		
		//}
		// NEED TO ADD: time stop
		// NEED TO ADD: clear animation as a feedback of completing a graph
		//NEED TO ADD:show "game over screen"(including scores,play again button, quit button)
		
	}
	
	public void ReloadAfterLoose()
	{
		//update score
		ss.UpdateScoreText(0 * spwn.no_edges);
		disappear_graph();
		spwn.first_flag = true;
		//if(!thread2.lose_flag){}
		spwn.SpawnCoin();
	}
	public void winGraph()
	{
		////Debug.Log("finish graph,yah");
		
		if (TranquilMode.modeselected())
		{
			//win_flag = true;
			////Debug.Log("KO ");
			disappear_graph();
			ss.UpdateScoreText(1000 * spwn.no_edges);
			GameRestart.transform.position = new Vector3(0, 0, 3);
		}
		else
		{
			//update score
			ss.UpdateScoreText(1000 * spwn.no_edges);
			win_time = tt.currentTime;
			win_flag = true;
			for (int i = 0; i < spwn.edges.Length; i++)
			{
				spwn.edges[i].enabled = false;
			}
			for (int i = 0; i < spwn.line_feedback.Length; i++)
				//spwn.line_feedback[i].SetWidth(0f, 0f);
				spwn.line_feedback[i].enabled = false;
			
		}
	}
	public  void disappear_graph()
	{
		if (eggIsOn && !TranquilMode.modeselected())
		{
			
			
			eggIsOn = false;
			/*Renderer rend = spwn.Coins [egg_num].GetComponent<Renderer> ();
                rend.material.shader = Shader.Find ("Specular");
                rend.material.SetColor ("_Color",Color.red);*/
			Transform eggt = spwn.Coins[egg_num].GetComponent<Transform>();
			eggt.localScale = orig_sc;
			spwn.starColor[egg_num].material.color = new Color(1, 1, 1);
			spwn.starLight[egg_num].color = new Color(1, 1, 1);
		}
		
		for (int i = 0; i < spwn.Coins.Length; i++)
		{
			Transform LALA = spwn.Coins[i].GetComponent<Transform>();
			LALA.localScale = orig_sc;
			
			spwn.Coins[i].SetActive(false);
			
		}
		for (int i = 0; i < spwn.edges.Length; i++)
		{
			spwn.edges[i].enabled = false;
		}
		for (int i = 0; i < spwn.line_feedback.Length; i++)
			spwn.line_feedback[i].enabled = false;
	}
}
