﻿using UnityEngine;
using System.Collections;
using Leap;

public class NewLeapInterface : MonoBehaviour
{

    // public Transform Sphere;
    public spawnItemNew1 SpawnGraph1;
    public spawnItem_new SpawnGraph2;
    public int click_ball = -1;
    public int click_ball2 = -1;
    private Transform Sphere;
    private Leap.Controller ctrl;
    private Leap.Frame frame;
    private Leap.ScreenTapGesture ScreenTap;
    private Leap.CircleGesture CircleGesture;
    private float offsetX = 4f;
    private float offsetY = 6f;
    private float ScaleFactor = 1.656f;
    private float ScaleFactorX = -1.156f;
    private float ScaleFactorY = -1.156f;
    private float ScaleFactorCoins = 1f;
    bool InputLeap = false;
    static Vector3 StoreLeapObjLoc = new Vector3();
    private Leap.Vector circleCenter;
    private Vector3 cameraRelative = new Vector3();
    private bool CircleGestureDetected = false;
    private bool visitedHereAlready = false;

    private static Vector3 GameobjectLoc = new Vector3(0, -250, 0);
    void Start()
    {
        ctrl = new Leap.Controller();
    }

    // Update is called once per frame
    void Update()
    {
        //tapdown ();
        if (Time.timeScale != 0)
        {
            frame = ctrl.Frame();
            Transform Cam = Camera.main.transform;
            Leap.Vector position = new Leap.Vector();
            Leap.Vector Direction = new Leap.Vector();
            Vector3 PosLeap = new Vector3();
            if (frame.Hands.Count > 0)
            {
                //Leap.Vector position = frame.Hands[0].StabilizedPalmPosition;
                Leap.FingerList Fingerlist = frame.Hands[0].Fingers;
                Leap.Finger Finger = Fingerlist.Frontmost;
                //print("Finger Id " + Finger.Id);
                //print("Averaging..........");
                int Limit = 10;
                //for (int i = 0; i < Limit; i++)
                {
                    position = Finger.StabilizedTipPosition;
                    //ScaleFactor = Mathf.Log10(4*ScaleFactor*position.x);
                    //print("leap position" + position);
                    //if(position.y < 60 )
                    //{
                    //    Vector3 PositionRelative = new Vector3(position.x * ScaleFactor, position.y * ScaleFactor, 0);
                    //    transform.localPosition = new Vector3(PositionRelative.x, PositionRelative.y - 20);
                    // }
                    //else
                    transform.localPosition = new Vector3(GameobjectLoc.x + (position.x * ScaleFactor), GameobjectLoc.y + (position.y * ScaleFactor), 2);
                    PosLeap = transform.localPosition;
                    cameraRelative = Cam.InverseTransformDirection(transform.position);
                }
                //PosLeap = PosLeap / Limit;
                //cameraRelative = cameraRelative / Limit;
                // print("average tip position  " + PosLeap);
                StoreLeapObjLoc = PosLeap;
                //Position = new Vector3(position.x * ScaleFactor * ScaleFactor2 * ScaleFactor3, position.y * ScaleFactor * ScaleFactor2 * ScaleFactor3, -10);
                // print("PosLeap " + PosLeap + "FixedSphere  " + Sphere.position);
                float range = 0.3f;
                for (int i = 0; i < SpawnGraph1.no_stars; i++)
                {
                    Sphere = SpawnGraph1.Coins[i].GetComponent<Transform>();
                    
                    //print("PosLeap " + PosLeap + "FixedSphere  " + Sphere.position + "Camera Ralative " + cameraRelative);
                    bool test = ((cameraRelative.x - range < Sphere.position.x)
                                 && (cameraRelative.x + range > Sphere.position.x))
                                    && ((cameraRelative.y - range < Sphere.position.y)
                                        && (cameraRelative.y + range > Sphere.position.y));
                    

                    if (test)
                    {
                            //print("Success and click on ball" + i);
                            click_ball = i;
                            Transform tro = SpawnGraph1.Coins[i].GetComponent<Transform>();
                        //tro.Rotate (Vector3.left, 90);
                        /*Vector3 n= new Vector3(20,20,20);
                    Transform T=Sphere.GetComponent<Transform>();
                    T.position=n;*/
                    }
                }

                for (int i = 0; i < SpawnGraph2.no_stars; i++)
                {
                    Sphere = SpawnGraph2.Coins[i].GetComponent<Transform>();
                    bool testGraph2 = ((cameraRelative.x - range < Sphere.position.x)
                                        && (cameraRelative.x + range > Sphere.position.x))
                                            && ((cameraRelative.y - range < Sphere.position.y)
                                                && (cameraRelative.y + range > Sphere.position.y));

                    if (testGraph2)
                    {
                        click_ball2 = i;
                        Transform tro = SpawnGraph2.Coins[i].GetComponent<Transform>();
                    }
                }




            }
        }

        //reduce frame rate for using time may be.. 
    }

}
