﻿using UnityEngine;
using System.Collections;

public class PlayIntro : MonoBehaviour {

	// Use this for initialization
    private MovieTexture temp;

	void Start () {
        temp = (MovieTexture)GetComponent<Renderer>().material.mainTexture;
        temp.Play();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (!(temp.isPlaying) || Input.GetMouseButtonDown(0))
        {
            Application.LoadLevel("Menu Scene");   
        }   
	}
}
